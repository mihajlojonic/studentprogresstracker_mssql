﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts;
using Entities.Extensions;
using Entities.Models;
using Entities.StoredProceduresUpdateModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPILabPractise_NetCore2.Controllers
{
    [Produces("application/json")]
    [Route("api/StudentGroup")]
    public class StudentGroupController : Controller
    {
        private ILoggerManager _logger;
        private IRepositoryWrapper _repoWrapper;

        public StudentGroupController(IRepositoryWrapper repoWrapper, ILoggerManager logger)
        {
            _logger = logger;
            _repoWrapper = repoWrapper;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                var studentsGroups = await _repoWrapper.StudentGroup.GetAllStudentGroupAsync();
                _logger.LogInfo($"Returned all StudentsGroups from database.");
                return Ok(studentsGroups);
            }
            catch(Exception ex)
            {
                _logger.LogError($"Something went wrong inside Get (AllStudentGroup) actoin: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpGet("{id}", Name = "StudentGroupsById")]
        public async Task<IActionResult> GetSudentsGroupsById(int id)
        {
            try
            {
                var studentGroup = await _repoWrapper.StudentGroup.GetStudentGroupByIdAsync(id);

                if(studentGroup.StudentGroupId.Equals(null))
                {
                    _logger.LogError($"StudentGroup with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                else
                {
                    _logger.LogInfo($"Returned studentGroup with id: {id}");
                    return Ok(studentGroup);
                }
            }
            catch(Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetStudentGroupById action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpPost]
        public async Task<IActionResult> CreateStudentGroup([FromBody]StudentGroup studentGroup)
        {
            try
            {
                if(studentGroup.IsObjectNull())
                {
                    _logger.LogError("StudentGroup object sent from client is null.");
                    return BadRequest("StudentGroup object is null");
                }

                if(!ModelState.IsValid)
                {
                    _logger.LogError("Invalid StudentGroup object send from client.");
                    return BadRequest("invalid model object");
                }

                await _repoWrapper.StudentGroup.CreateStudentGroupAsync(studentGroup);

                return CreatedAtAction("StudentGroupsById", new { id = studentGroup.StudentGroupId }, studentGroup);
            }
            catch(Exception ex)
            {
                _logger.LogError($"Something wrong inside CreateStudentGroup action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
        
        [HttpPut("swap", Name = "SwapAction")]
        public async Task<IActionResult> SwapStudentGroup([FromBody]StudentGroupSwap studentGroupSwap)
        {
            try
            {
                if(studentGroupSwap == null)
                {
                    _logger.LogError("StudentGroupSwap object sent from client is null.");
                    return BadRequest("StudentGroupSwap object is null");
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid studentGroupSwap object sent from client.");
                    return BadRequest("Invalid model object");
                }

                await _repoWrapper.StudentGroup.SwapStudentsGroup(studentGroupSwap);

                return NoContent();

            }
            catch(Exception ex)
            {
                _logger.LogError($"Something went wrong inside SwapStudentGroup action: {ex.Message}");
                return StatusCode(500, "internal server error");
            }

        }
        
        [HttpPut("sendto", Name = "SendToActionStudent")]
        public async Task<IActionResult> SendToStudentGroup([FromBody]StudentGroupSendTo studentGroupSendTo)
        {
            try
            {
                if (studentGroupSendTo == null)
                {
                    _logger.LogError("StudentGroupSendTo object sent from client is null.");
                    return BadRequest("StudentGroupSendTo object is null");
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid StudentGroupSendTo object sent from client.");
                    return BadRequest("Invalid model object");
                }

                await _repoWrapper.StudentGroup.SendToStudentGroup(studentGroupSendTo);

                return NoContent();

            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside studentGroupSendTo action: {ex.Message}");
                return StatusCode(500, "internal server error");
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateStudentGroup(int id, [FromBody]StudentGroup studentGroup)
        {
            try
            {
                if (studentGroup.IsObjectNull())
                {
                    _logger.LogError("StudentGroup object sent from client is null.");
                    return BadRequest("StudentGroup object is null");
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid studentGroup object sent from client.");
                    return BadRequest("Invalid model object");
                }

                var dbStudentGroup = await _repoWrapper.StudentGroup.GetStudentGroupByIdAsync(id);

                if (dbStudentGroup.StudentGroupId.Equals(null))
                {
                    _logger.LogError($"StudentGroup with id: {id}, hasn't been found in db.");
                    return NotFound();
                }

                await _repoWrapper.StudentGroup.UpdateStudentGroupAsync(dbStudentGroup, studentGroup);

                return NoContent();
            }
            catch(Exception ex)
            {
                _logger.LogError($"Something went wrong inside UpdateStudentGroup action: {ex.Message}");
                return StatusCode(500, "internal server error");
            }
        }
        
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteStudentGroup(int id)
        {
            try
            {
                var studentGroup = await _repoWrapper.StudentGroup.GetStudentGroupByIdAsync(id);

                if (studentGroup.StudentGroupId.Equals(null))
                {
                    _logger.LogError($"StudentGroup with id: {id}, hasn't been found in db.");
                    return NotFound();
                }

                await _repoWrapper.StudentGroup.DeleteStudentGroupAsync(studentGroup);

                return NoContent();
            }
            catch(Exception ex)
            {
                _logger.LogError($"Somenthing went wrong inside Delete action: {ex.Message}");
                return StatusCode(500, "internal server error");
            }
        }
    }
}