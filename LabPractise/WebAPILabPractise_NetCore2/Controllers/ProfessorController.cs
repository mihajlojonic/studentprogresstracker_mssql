﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts;
using Entities.Extensions;
using Entities.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPILabPractise_NetCore2.Controllers
{
    [Produces("application/json")]
    [Route("api/Professor")]
    public class ProfessorController : Controller
    {
        private ILoggerManager _logger;
        private IRepositoryWrapper _repoWrapper;

        public ProfessorController(IRepositoryWrapper repoWrapper, ILoggerManager logger)
        {
            _logger = logger;
            _repoWrapper = repoWrapper;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                var professors = await _repoWrapper.Professor.GetAllProfessorsAsync();
                _logger.LogInfo($"Returned all professors from database.");
                return Ok(professors);
            }
            catch(Exception ex)
            {
                _logger.LogError($"Something went wrong inside Get (AllProfessor) action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpGet("{id}", Name = "ProfessorById")]
        public async Task<IActionResult> GetProfessorById(int id)
        {
            try
            {
                var professor = await _repoWrapper.Professor.GetProfessorByIdAsync(id);

                if(professor.ProfessorId.Equals(null))
                {
                    _logger.LogError($"Professor with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                else
                {
                    _logger.LogInfo($"Returned professor with id: {id}");
                    return Ok(professor);
                }
            }
            catch(Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetProfessorById action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpGet("{id}/group", Name = "ProfessorPlusGroupForLab")]
        public async Task<IActionResult> GetProfessorGroupForLabAsync(int id)
        {
            try
            {
                var professor = await _repoWrapper.Professor.GetProfessorGroupForLabAsync(id);
                
                if(professor.ProfessorId.Equals(null))
                {
                    _logger.LogError($"ProfessorGroupForLab with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                else
                {
                    _logger.LogInfo($"Returned ProfessorGroupForLab for id: {id}");
                    return Ok(professor);
                }
            }
            catch(Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetProfessorGroupForLabAsync action: {ex.Message}");
                return StatusCode(500, "Interanal server error");
            }
        } 

        [HttpGet("{id}/course", Name = "ProfessorPlusCourse")]
        public async Task<IActionResult> GetProfessorCourseAsync(int id)
        {
            try
            {
                var professor = await _repoWrapper.Professor.GetProfessorCourseAsync(id);

                if(professor.ProfessorId.Equals(null))
                {
                    _logger.LogError($"ProfessorCourse with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                else
                {
                    _logger.LogInfo($"Returned ProfessorCourse for id: {id}");
                    return Ok(professor);
                }
            }
            catch(Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetProfessorGroupForLabAsync action: {ex.Message}");
                return StatusCode(500, "internal server error");
            }
        }
        
        [HttpPost]
        public async Task<IActionResult> CreateProfessor([FromBody]Professor professor)
        {
            try
            {
                if(professor.IsObjectNull())
                {
                    _logger.LogError("Professor object sent from client is null.");
                    return BadRequest("Course object is null");
                }

                if(!ModelState.IsValid)
                {
                    _logger.LogError("Invalid professor object sent from client.");
                    return BadRequest("Invalid model object");
                }

                await _repoWrapper.Professor.CreateProfessorAsync(professor);

                return CreatedAtAction("ProfessorById", new { id = professor.ProfessorId }, professor);
            }
            catch(Exception ex)
            {
                _logger.LogError($"Something went wrond inside CreateProfessor action: {ex.Message}");
                return StatusCode(500, "Internal sever error");
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateProfessor(int id, [FromBody]Professor professor)
        {
            try
            {
                if(professor.IsObjectNull())
                {
                    _logger.LogError("Professor object sent from client is null.");
                    return BadRequest("Professor object is null");
                }

                if(!ModelState.IsValid)
                {
                    _logger.LogError("Invalid professor object sent from client.");
                    return BadRequest("Invalid model object");
                }

                var dbProfessor = await _repoWrapper.Professor.GetProfessorByIdAsync(id);

                if(dbProfessor.ProfessorId.Equals(null))
                {
                    _logger.LogError($"Professor with id: {id}, hasn't been found in db.");
                    return NotFound();
                }

                await _repoWrapper.Professor.UpdateProfessorAsync(dbProfessor, professor);

                return NoContent();
            }
            catch(Exception ex)
            {
                _logger.LogError($"Something went wrong inside UpdateProfessor action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProfessor(int id)
        {
            try
            {
                var professor = await _repoWrapper.Professor.GetProfessorByIdAsync(id);

                if (professor.ProfessorId.Equals(null))
                {
                    _logger.LogError($"Professor with id: {id}, hasn't been found in db.");
                    return NotFound();
                }

                await _repoWrapper.Professor.DeleteProfessorAsync(professor);

                return NoContent();
            }
            catch(Exception ex)
            {
                _logger.LogError($"Something went wrong inside Delete action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
    }
}