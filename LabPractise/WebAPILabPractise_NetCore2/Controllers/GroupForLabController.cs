﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPILabPractise_NetCore2.Controllers
{
    [Produces("application/json")]
    [Route("api/GroupForLab")]
    public class GroupForLabController : Controller
    {
        private ILoggerManager _logger;
        private IRepositoryWrapper _repoWrapper;

        public GroupForLabController(IRepositoryWrapper repoWrapper, ILoggerManager logger)
        {
            _logger = logger;
            _repoWrapper = repoWrapper;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                var groups = await _repoWrapper.GroupForLab.GetAllGroupsForLabAsync();
                _logger.LogInfo($"Returned all groups from database.");
                return Ok(groups);
            }
            catch(Exception ex)
            {
                _logger.LogError($"Something went wrond inside Get (AllgroupsForLab) action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpGet("{id}", Name = "GroupForLabById")]
        public async Task<IActionResult> GetGroupForLabById(int id)
        {
            try
            {
                var group = await _repoWrapper.GroupForLab.GetGroupForLabByIdAsync(id);

                if(group.GroupForLabId.Equals(null))
                {
                    _logger.LogError($"GroupForLab with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                else
                {
                    _logger.LogInfo($"Returned group with id: {id}");
                    return Ok(group);
                }
            }
            catch(Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetGroupForLabById action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpGet("{courseId}/course", Name = "GroupForLabByCourseId")]
        public async Task<IActionResult> GetGroupForLabByCourseId(int courseId)
        {
            try
            {
                var groups = await _repoWrapper.GroupForLab.GetGroupForLabByCourseIdAsync(courseId);

                if(groups.Count() == 0)
                {
                    _logger.LogError($"GroupForLab with courseId: {courseId}, hasn't been found in db.");
                    return NotFound();
                }

                _logger.LogInfo("Returned all groups by courseId from db.");
                return Ok(groups);
            }
            catch(Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetGroupForLabByCourseId action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpGet("{courseId}/full", Name = "GroupForLabByCourseIdWithStudentsAndProfessors")]
        public async Task<IActionResult> GetGroupForLabByCourseIdFull(int courseId)
        {
            try
            {
                var groups = await _repoWrapper.GroupForLab.GetGroupForLabFullAsync(courseId);

                if(groups == null)
                {
                    _logger.LogError($"GroupsFull with course id: {courseId}, hasn't been found in db.");
                    return NotFound();
                }
                else
                {
                    _logger.LogInfo($"Returned GroupsFull for courseId: {courseId}");
                    return Ok(groups);
                }
            }
            catch(Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetGroupForLabByCourseIdFull action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
    }
}