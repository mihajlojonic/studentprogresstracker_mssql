﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts;
using Entities.Extensions;
using Entities.Models;
using Entities.StoredProceduresUpdateModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPILabPractise_NetCore2.Controllers
{
    [Produces("application/json")]
    [Route("api/ProfessorGroup")]
    public class ProfessorGroupController : Controller
    {
        ILoggerManager _logger;
        IRepositoryWrapper _repoWrapper;

        public ProfessorGroupController(IRepositoryWrapper repoWrapper, ILoggerManager logger)
        {
            _logger = logger;
            _repoWrapper = repoWrapper;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                var professorsGroups = await _repoWrapper.ProfessorGroup.GetAllProfessorGroupAsync();
                _logger.LogInfo($"Returned all ProfessorsGroups from database.");
                return Ok(professorsGroups);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside Get (AllProfessorGroup) actoin: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpGet("{id}", Name = "ProfessorsGroupsById")]
        public async Task<IActionResult> GetProfessorsGroupsById(int id)
        {
            try
            {
                var professorsGroup = await _repoWrapper.ProfessorGroup.GetProfessorGroupByIdAsync(id);

                if (professorsGroup.ProfessorGroupId.Equals(null))
                {
                    _logger.LogError($"ProfessorGroup with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                else
                {
                    _logger.LogInfo($"Returned professorGroup with id: {id}");
                    return Ok(professorsGroup);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetProfessorGroupById action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpPost]
        public async Task<IActionResult> CreateProfessorGroup([FromBody]ProfessorGroup professorGroup)
        {
            try
            {
                if (professorGroup.IsObjectNull())
                {
                    _logger.LogError("professorGroup object sent from client is null.");
                    return BadRequest("professorGroup object is null");
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid professorGroup object send from client.");
                    return BadRequest("invalid model object");
                }

                await _repoWrapper.ProfessorGroup.CreateProfessorGroupAsync(professorGroup);

                return CreatedAtAction("ProfessorsGroupsById", new { id = professorGroup.ProfessorGroupId}, professorGroup);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something wrong inside CreateProfessorGroup action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpPut("sendto", Name = "SendToActionProfessor")]
        public async Task<IActionResult> SendToProfessorGroup([FromBody]ProfessorGroupSendTo professorGroupSendTo)
        {
            try
            {
                if (professorGroupSendTo == null)
                {
                    _logger.LogError("ProfessorGroupSendTo object sent from client is null.");
                    return BadRequest("ProfessorGroupSendTo object is null");
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid ProfessorGroupSendTo object sent from client.");
                    return BadRequest("Invalid model object");
                }

                await _repoWrapper.ProfessorGroup.SendToProfessorGroup(professorGroupSendTo);

                return NoContent();

            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside ProfessorGroupSendTo action: {ex.Message}");
                return StatusCode(500, "internal server error");
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateProfessorGroup(int id, [FromBody]ProfessorGroup professorGroup)
        {
            try
            {
                if (professorGroup.IsObjectNull())
                {
                    _logger.LogError("ProfessorGroup object sent from client is null.");
                    return BadRequest("ProfessorGroup object is null");
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid professorGroup object sent from client.");
                    return BadRequest("Invalid model object");
                }

                var dbProfessorGroup = await _repoWrapper.ProfessorGroup.GetProfessorGroupByIdAsync(id);

                if (dbProfessorGroup.ProfessorGroupId.Equals(null))
                {
                    _logger.LogError($"ProfessorGroup with id: {id}, hasn't been found in db.");
                    return NotFound();
                }

                await _repoWrapper.ProfessorGroup.UpdateProfessorGroupAsync(dbProfessorGroup, professorGroup);

                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside UpdateProfessorGroup action: {ex.Message}");
                return StatusCode(500, "internal server error");
            }
        }
        
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProfessorGroup(int id)
        {
            try
            {
                var professorGroup = await _repoWrapper.ProfessorGroup.GetProfessorGroupByIdAsync(id);

                if (professorGroup.ProfessorGroupId.Equals(null))
                {
                    _logger.LogError($"ProfessorGroup with id: {id}, hasn't been found in db.");
                    return NotFound();
                }

                await _repoWrapper.ProfessorGroup.DeleteProfessorGroupAsync(professorGroup);

                return NoContent();
            }
            catch(Exception ex)
            {
                _logger.LogError($"Somenthing went wrong inside Delete action (DeleteProfessorGroup): {ex.Message}");
                return StatusCode(500, "internal server error");
            }
        }
    }
}