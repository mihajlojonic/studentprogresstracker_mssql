﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Entities.Models;
using Entities.Extensions;

namespace WebAPILabPractise_NetCore2.Controllers
{
    [Produces("application/json")]
    [Route("api/Department")]
    public class DepartmentController : Controller
    {
        private ILoggerManager _logger;
        private IRepositoryWrapper _repoWrapper;

        public DepartmentController(IRepositoryWrapper repositoryWrapper, ILoggerManager loggerManager)
        {
            this._logger = loggerManager;
            this._repoWrapper = repositoryWrapper;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                var departments = await _repoWrapper.Department.FindAllAsync();
                _logger.LogInfo("Returned all departments from database.");
                return Ok(departments);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went worong inside Get (AllDepartments) action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpGet("{id}", Name = "DepartmnetById")]
        public async Task<IActionResult> GetDepartmentById(int id)
        {
            try
            {
                var department = await _repoWrapper.Department.GetDepartmentById(id);

                if(department.DepartmentId.Equals(null))
                {
                    _logger.LogError($"Deparmtnet with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                else
                {
                    _logger.LogInfo($"Returned department with id: {id}");
                    return Ok(department);
                }
            }
            catch(Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetDeparmentById action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpPost]
        public async Task<IActionResult> CreateDepartment([FromBody]Department department)
        {
            try
            {
                if (department.IsObjectNull())
                {
                    _logger.LogError("Department object sent from client is null.");
                    return BadRequest("Deparmtent object is null");
                }
                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid deparmtent object sent from client.");
                    return BadRequest("Invalid model object");
                }
                await _repoWrapper.Department.CreateDepartmentAsync(department);

                return CreatedAtRoute("CourseById", new { id = department.DepartmentId }, department);
            }
            catch(Exception ex)
            {
                _logger.LogError($"Something went wrong inside CreateDeparmtnet action: {ex.Message}");
                return StatusCode(500, "Internal server error" + ex.ToString());
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateDepartment(int id, [FromBody]Department department)
        {
            try
            {
                if (department.IsObjectNull())
                {
                    _logger.LogError("Department object sent from client is null.");
                    return BadRequest("Department object is null");
                }


                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid department object sent from client.");
                    return BadRequest("Invalid model object");
                }

                var dbDepartment = await _repoWrapper.Department.GetDepartmentById(id);

                if (dbDepartment.DepartmentId.Equals(null))
                {
                    _logger.LogError($"Department with id: {id}, hasn't been found in db.");
                    return NotFound();
                }

                await _repoWrapper.Department.UpdateDepartmentAsync(dbDepartment, department);

                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside UpdateDepartment action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteDepartment(int id)
        {
            try
            {
                var department = await _repoWrapper.Department.GetDepartmentById(id);
                if (department.DepartmentId.Equals(null))
                {
                    _logger.LogError($"Department with id: {id}, hasn't been found in db.");
                    return NotFound();
                }

                await _repoWrapper.Department.DeleteDepartmnetAsync(department);

                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside DeleteDepartment action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
    }
}