﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts;
using Entities.Extensions;
using Entities.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPILabPractise_NetCore2.Controllers
{
    [Produces("application/json")]
    [Route("api/Course")]
    public class CourseController : Controller
    {
        private ILoggerManager _logger;
        private IRepositoryWrapper _repoWrapper;

        public CourseController(IRepositoryWrapper repoWrapper, ILoggerManager logger)
        {
            _logger = logger;
            _repoWrapper = repoWrapper;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                var courses = await _repoWrapper.Course.GetAllCoursesAsync();
                _logger.LogInfo($"Returned all courses from database.");
                return Ok(courses);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside Get (AllCourses) action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpGet("{id}", Name = "CourseById")]
        public async Task<IActionResult> GetCourseById(int id)
        {
            try
            {
                var course = await _repoWrapper.Course.GetCourseByIdAsync(id);

                if (course.CourseId.Equals(null))
                {
                    _logger.LogError($"Course with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                else
                {
                    _logger.LogInfo($"Returned course with id: {id}");
                    return Ok(course);
                }
                    
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetCourseById action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
            
        [HttpGet("{id}/professor", Name = "CoursesPlusProfessor")]
        public async Task<IActionResult> GetCourseProfessors(int id)
        {
            try
            {
                var course = await _repoWrapper.Course.GetCourseProfessorsAsync(id);

                if (course.CourseId.Equals(null))
                {
                    _logger.LogError($"CoursePlusProfessors with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                else
                {
                    _logger.LogInfo($"Returned CoursePlusPforessors for id: {id}");
                    return Ok(course);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetCourseProfessors action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
        
        [HttpGet("{id}/student", Name = "CoursesPlusStudent")]
        public async Task<IActionResult> GetCourseStudentsAsync(int id)
        {
            try
            {
                var course = await _repoWrapper.Course.GetCourseStudentsAsync(id);

                if (course.CourseId.Equals(null))
                {
                    _logger.LogError($"CoursePlusStudents with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                else
                {
                    _logger.LogInfo($"Returned CoursePlusStudents for id: {id}");
                    return Ok(course);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetCourseStudentsAsync action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
        
        [HttpGet("{id}/full", Name = "CoursesPlusFull")]
        public async Task<IActionResult> GetCourseFullAsync(int id)
        {
            try
            {
                var course = await _repoWrapper.Course.GetCourseFullAsync(id);

                if (course.CourseId.Equals(null))
                {
                    _logger.LogError($"CourseFull with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                else
                {
                    _logger.LogInfo($"Returned CourseFull for id: {id}");
                    return Ok(course);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetCourseFullAsync action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
        
        [HttpPost]
        public async Task<IActionResult> CreateCourse([FromBody]Course course)
        {
            try
            {
                if (course.IsObjectNull())
                {
                    _logger.LogError("Course object sent from client is null.");
                    return BadRequest("Course object is null");
                }
                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid course object sent from client.");
                    return BadRequest("Invalid model object");
                }
                await _repoWrapper.Course.CreateCourseAsync(course);

                return CreatedAtRoute("CourseById", new { id = course.CourseId }, course);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside CreateCourse action: {ex.Message}");
                return StatusCode(500, "Internal server error" + ex.ToString());
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateCourse(int id, [FromBody]Course course)
        {
            try
            {
                if (course.IsObjectNull())
                {
                    _logger.LogError("Course object sent from client is null.");
                    return BadRequest("Course object is null");
                }
                

                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid course object sent from client.");
                    return BadRequest("Invalid model object");
                }
                
                var dbCourse = await _repoWrapper.Course.GetCourseByIdAsync(id);

                if (dbCourse.CourseId.Equals(null))
                {
                    _logger.LogError($"Course with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                    
                await _repoWrapper.Course.UpdateCourseAsync(dbCourse, course);

                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside UpdateCourse action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
        
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCourse(int id)
        {
            try
            {
                var course = await _repoWrapper.Course.GetCourseByIdAsync(id);
                if (course.CourseId.Equals(null))
                {
                    _logger.LogError($"Course with id: {id}, hasn't been found in db.");
                    return NotFound();
                }

                await _repoWrapper.Course.DeleteCourseAsync(course);

                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside DeleteCourse action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
    }
}