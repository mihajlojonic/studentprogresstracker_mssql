﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts;
using Entities.Extensions;
using Entities.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPILabPractise_NetCore2.Controllers
{
    [Produces("application/json")]
    [Route("api/Student")]
    public class StudentController : Controller
    {
        private ILoggerManager _logger;
        private IRepositoryWrapper _repoWrapper;

        public StudentController(IRepositoryWrapper repoWrapper, ILoggerManager logger)
        {
            _logger = logger;
            _repoWrapper = repoWrapper;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                var students = await _repoWrapper.Student.GetAllStudentsAsync();
                _logger.LogInfo($"Returned all students from database.");
                return Ok(students);
            }
            catch(Exception ex)
            {
                _logger.LogError($"Something went wrond inside Get (AllStudent) action: {ex.Message}");
                return StatusCode(500, "internal server error");
            }
        }

        [HttpGet("{id}", Name = "StudentById")]
        public async Task<IActionResult> GetStudentById(int id)
        {
            try
            {
                var student = await _repoWrapper.Student.GetStudentByIdAsync(id);

                if(student.StudentId.Equals(null))
                {
                    _logger.LogError($"Student with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                else
                {
                    _logger.LogInfo($"Returned student with id: {id}");
                    return Ok(student);
                }
            }
            catch(Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetStudentById action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpGet("{id}/group", Name = "StudentPlusGroupForLab")]
        public async Task<IActionResult> GetStudentGroupForLab(int id)
        {
            try
            {
                var student = await _repoWrapper.Student.GetStudentGroupForLabAsync(id);

                if(student.StudentId.Equals(null))
                {
                    _logger.LogError($"StudentGroupForLab with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                else
                {
                    _logger.LogInfo($"Returned StudentGroupForLab for id: {id}");
                    return Ok(student);
                }
            }
            catch(Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetStudentGroupForLabAsync action: {ex.Message}");
                return StatusCode(500, "Interanal server error");
            }
        }

        [HttpGet("{id}/course", Name = "StudentPlusCourse")]
        public async Task<IActionResult> GetStudentCourseAsync(int id)
        {
            try
            {
                var student = await _repoWrapper.Student.GetStudentCourseAsync(id);

                if (student.StudentId.Equals(null))
                {
                    _logger.LogError($"StudentCourse with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                else
                {
                    _logger.LogInfo($"Returned StudentCourse for id: {id}");
                    return Ok(student);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetStudentGroupForLabAsync action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpPost]
        public async Task<IActionResult> CreateStudent([FromBody]Student student)
        {
            try
            {
                if(student.IsObjectNull())
                {
                    _logger.LogError("Student object sent from client is null.");
                    return BadRequest("Student object is null");
                }

                if(!ModelState.IsValid)
                {
                    _logger.LogError("Invalid student object send from client.");
                    return BadRequest("Invalid model object");
                }

                await _repoWrapper.Student.CreateStudentAsync(student);

                return CreatedAtAction("StudentById", new { id = student.StudentId }, student);
            }
            catch(Exception ex)
            {
                _logger.LogError($"Somthingwent wrong inside CreateStudent action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateStudent(int id, [FromBody]Student student)
        {
            try
            {
                if(student.IsObjectNull())
                {
                    _logger.LogError("Student object sent from client is null.");
                    return BadRequest("Student object is null");
                }

                if(!ModelState.IsValid)
                {
                    _logger.LogError("Invalid student object sent from client.");
                    return BadRequest("Invalid model object");
                }

                var dbStudent = await _repoWrapper.Student.GetStudentByIdAsync(id);

                if(dbStudent.StudentId.Equals(null))
                {
                    _logger.LogError($"Student with id: {id}, hasn't been found in db.");
                    return NotFound();
                }

                await _repoWrapper.Student.UpdateStudentAsync(dbStudent, student);

                return NoContent();
            }
            catch(Exception ex)
            {
                _logger.LogError($"Something went wrong inside UpdateStudent action: {ex.Message}");
                return StatusCode(500, "internal server error");
            }
        }

        
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteStudent(int id)
        {
            try
            {
                var student = await _repoWrapper.Student.GetStudentByIdAsync(id);

                if(student.StudentId.Equals(null))
                {
                    _logger.LogError($"Student with id: {id}, hasn't been found in db.");
                    return NotFound();
                }

                await _repoWrapper.Student.DeleteStudentAsync(student);

                return NoContent();
            }
            catch(Exception ex)
            {
                _logger.LogError($"Somenthing went wrong inside Delete action: {ex.Message}");
                return StatusCode(500, "internal server error");
            }
        }
    }
}