﻿using Entities.Models;
using Entities.StoredProceduresUpdateModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Contracts
{
    public interface IStudentGroupRepository
    {
        Task<IEnumerable<StudentGroup>> GetAllStudentGroupAsync();
        Task<StudentGroup> GetStudentGroupByIdAsync(int studentGroupId);
        Task SwapStudentsGroup(StudentGroupSwap studentGroupSwap);
        Task SendToStudentGroup(StudentGroupSendTo studentGroupSendTo);
        Task CreateStudentGroupAsync(StudentGroup studentGroup);
        Task UpdateStudentGroupAsync(StudentGroup dbStudentGroup, StudentGroup studentGroup);
        Task DeleteStudentGroupAsync(StudentGroup studentGroup);
    }
}
