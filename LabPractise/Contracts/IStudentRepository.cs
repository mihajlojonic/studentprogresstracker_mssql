﻿using Entities.ExtendedModels;
using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Contracts
{
    public interface IStudentRepository
    {
        Task<IEnumerable<Student>> GetAllStudentsAsync();
        Task<Student> GetStudentByIdAsync(int studentId);
        Task<StudentExtended> GetStudentGroupForLabAsync(int studentId);
        Task<StudentExtended> GetStudentCourseAsync(int studentId);
        Task CreateStudentAsync(Student student);
        Task UpdateStudentAsync(Student dbStudent, Student student);
        Task DeleteStudentAsync(Student student);
    }
}
