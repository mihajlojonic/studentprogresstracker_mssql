﻿using Entities.ExtendedModels;
using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Contracts
{
    public interface IProfessorRepository : IRepositoryBase<Professor>
    {
        Task<IEnumerable<Professor>> GetAllProfessorsAsync();
        Task<Professor> GetProfessorByIdAsync(int professorId);
        Task<ProfessorExtended> GetProfessorGroupForLabAsync(int professorId);
        Task<ProfessorExtended> GetProfessorCourseAsync(int professorId);
        Task CreateProfessorAsync(Professor professor);
        Task UpdateProfessorAsync(Professor dbProfessor, Professor professor);
        Task DeleteProfessorAsync(Professor professor);
    }
}
