﻿using Entities.Models;
using Entities.StoredProceduresUpdateModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Contracts
{
    public interface IProfessorGroupRepository
    {
        Task<IEnumerable<ProfessorGroup>> GetAllProfessorGroupAsync();
        Task<ProfessorGroup> GetProfessorGroupByIdAsync(int professorGroupId);
        Task SendToProfessorGroup(ProfessorGroupSendTo professorGroupSendTo);
        Task CreateProfessorGroupAsync(ProfessorGroup professorGroup);
        Task UpdateProfessorGroupAsync(ProfessorGroup dbProfessorGroup, ProfessorGroup professorGroup);
        Task DeleteProfessorGroupAsync(ProfessorGroup professorGroup);
    }
}
