﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contracts
{
    public interface IRepositoryWrapper
    {
        ICourseRepository Course { get; }
        IGroupForLabRepository GroupForLab { get; }
        IDepartmentRepository Department { get; }
        IProfessorRepository Professor { get; }
        IStudentRepository Student { get; }
        IStudentGroupRepository StudentGroup { get;  }
        IProfessorGroupRepository ProfessorGroup { get; }
    }
}
