﻿using Entities.ExtendedModels;
using Entities.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Contracts
{
    public interface IGroupForLabRepository : IRepositoryBase<GroupForLab>
    {
        Task<IEnumerable<GroupForLab>> GetAllGroupsForLabAsync();
        Task<GroupForLab> GetGroupForLabByIdAsync(int groupId);
        Task<IEnumerable<GroupForLab>> GetGroupForLabByCourseIdAsync(int courseId);
        Task<IEnumerable<GroupForLabExtended>> GetGroupForLabFullAsync(int courseId);
    }
}
