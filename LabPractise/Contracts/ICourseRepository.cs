﻿using Entities.ExtendedModels;
using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Contracts
{
    public interface ICourseRepository : IRepositoryBase<Course>
    {
        Task<IEnumerable<Course>> GetAllCoursesAsync();
        Task<Course> GetCourseByIdAsync(int courseId);
        Task<CourseExtended> GetCourseProfessorsAsync(int courseId);
        Task<CourseExtended> GetCourseStudentsAsync(int courseId);
        Task<CourseExtended> GetCourseFullAsync(int courseId);
        Task CreateCourseAsync(Course course);
        Task UpdateCourseAsync(Course dbCourse, Course Course);
        Task DeleteCourseAsync(Course course);
    }
}
