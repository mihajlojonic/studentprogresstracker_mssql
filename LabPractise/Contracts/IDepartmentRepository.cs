﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Contracts
{
    public interface IDepartmentRepository : IRepositoryBase<Department>
    {
        Task<IEnumerable<Department>> GetAllDepartmentsAsync();
        Task<Department> GetDepartmentById(int id);

        Task CreateDepartmentAsync(Department department);
        Task UpdateDepartmentAsync(Department dbDepartment, Department department);
        Task DeleteDepartmnetAsync(Department department);
    }
}
