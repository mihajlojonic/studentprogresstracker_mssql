﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Entities.StoredProceduresUpdateModels
{
    public class StudentGroupSendTo
    {
        public int StudentId          { get; set; }
        public int StudentNewGroupId  { get; set; }
        public int StudentPrevGroupId { get; set; }
    }
}
