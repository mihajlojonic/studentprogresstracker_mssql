﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.StoredProceduresUpdateModels
{
    public class ProfessorGroupSendTo
    {
        public int ProfessorId { get; set; }
        public int ProfessorNewGroupId { get; set; }
        public int ProfessorPrevGroupId { get; set; }
    }
}
