﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.StoredProceduresUpdateModels
{
    public class StudentGroupSwap
    {
        public int Student1_StudentId     { get; set; }
        public int Student1_GroupForLabId { get; set; }
        public int Student2_StudentId     { get; set; }
        public int Student2_GroupForLabId { get; set; }
    }
}
