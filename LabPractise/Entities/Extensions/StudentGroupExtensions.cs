﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Extensions
{
    public static class StudentGroupExtensions
    {
        public static void Map(this StudentGroup dbStudentGroup, StudentGroup studentGroup)
        {
            dbStudentGroup.StudentGroupId = studentGroup.StudentGroupId;
            dbStudentGroup.StudentId = studentGroup.StudentId;
            dbStudentGroup.GroupForLabId = studentGroup.GroupForLabId;
        }
    }
}
