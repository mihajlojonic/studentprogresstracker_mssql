﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Extensions
{
    public static class DepartmentExtensions
    {
        public static void Map(this Department dbDepartment, Department Department)
        {
            dbDepartment.DepartmentId = Department.DepartmentId;
            dbDepartment.DepartmentName = Department.DepartmentName;
        }

    }
}
