﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Extensions
{
    public static class CourseExtensions
    {
        public static void Map(this Course dbCourse, Course Course)
        {
            dbCourse.CourseId = Course.CourseId;
            dbCourse.CourseName = Course.CourseName;
            dbCourse.DepartmentId = Course.DepartmentId;
            dbCourse.Description = Course.Description;
            dbCourse.Description2 = Course.Description2;
            dbCourse.ShortName = Course.ShortName;
            dbCourse.Year = Course.Year;
        }
    }
}
