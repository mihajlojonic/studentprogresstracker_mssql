﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Extensions
{
    public static class ProfessorGroupExtensions
    {
        public static void Map(this ProfessorGroup dbProfessorGroup, ProfessorGroup professorGroup)
        {
            dbProfessorGroup.ProfessorGroupId = professorGroup.ProfessorGroupId;
            dbProfessorGroup.ProfessorId = professorGroup.ProfessorId;
            dbProfessorGroup.GroupForLabId = professorGroup.GroupForLabId;
        }
    }
}
