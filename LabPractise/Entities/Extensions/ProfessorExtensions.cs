﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Extensions
{
    public static class ProfessorExtensions
    {
        public static void Map(this Professor dbProfessor, Professor professor)
        {
            dbProfessor.ProfessorId = professor.ProfessorId;
            dbProfessor.FirstName = professor.FirstName;
            dbProfessor.LastName = professor.LastName;
            dbProfessor.ProfessorRank = professor.ProfessorRank;
        }

    }
}
