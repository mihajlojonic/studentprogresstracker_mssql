﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Extensions
{
    public static class StudentExtensions
    {
        public static void Map(this Student dbStudent, Student student)
        {
            dbStudent.StudentId = student.StudentId;
            dbStudent.FirstName = student.FirstName;
            dbStudent.LastName = student.LastName;
            dbStudent.Address = student.Address;
            dbStudent.BirthDate = student.BirthDate;
            dbStudent.BirthPlace = student.BirthPlace;
            dbStudent.Phone = student.Phone;
        }
    }
}
