﻿using Entities.Intefaces;
using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.ExtendedModels
{
    public class CourseExtended : IEntity
    {
        public int CourseId { get; set; }
        public string CourseName { get; set; }
        public string ShortName { get; set; }
        public int Year { get; set; }
        public string Description { get; set; }
        public int DepartmentId { get; set; }
        public string Description2 { get; set; }

        public IEnumerable<Professor> ProfessorsOnCourse { get; set; }
        public IEnumerable<Student> StudentsOnCourse { get; set; }
        public IEnumerable<GroupForLab> GroupsForLabOnCourse { get; set; }

        public CourseExtended(Course course)
        {
            CourseId = course.CourseId;
            CourseName = course.CourseName;
            ShortName = course.ShortName;
            Year = course.Year;
            Description = course.Description;
            Description2 = course.Description2;
            DepartmentId = course.DepartmentId;
        }
    }
}
