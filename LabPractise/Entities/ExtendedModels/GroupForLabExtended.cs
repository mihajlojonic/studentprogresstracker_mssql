﻿using Entities.Intefaces;
using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.ExtendedModels
{
    public class GroupForLabExtended : IEntity
    {
        public int GroupForLabId { get; set; }
        public int? CourseId { get; set; }
        public string DescriptionGroup { get; set; }
        public byte NoGroup { get; set; }

        public IEnumerable<Professor> ProfessorsForGroup { get; set; }
        public IEnumerable<Student> StudentsForGroup { get; set; }

        public GroupForLabExtended(GroupForLab groupForLab)
        {
            GroupForLabId = groupForLab.GroupForLabId;
            CourseId = groupForLab.CourseId;
            DescriptionGroup = groupForLab.DescriptionGroup;
            NoGroup = groupForLab.NoGroup;
        }
    }
}
