﻿using Entities.Intefaces;
using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.ExtendedModels
{
    public class StudentExtended : IEntity
    {
        public int StudentId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? BirthDate { get; set; }
        public string BirthPlace { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }

        public IEnumerable<Course> StudentCourse { get; set; }
        public IEnumerable<GroupForLab> StudentGroupForLab { get; set; }

        public StudentExtended(Student student)
        {
            StudentId = student.StudentId;
            FirstName = student.FirstName;
            LastName = student.LastName;
            BirthDate = student.BirthDate;
            BirthPlace = student.BirthPlace;
            Address = student.Address;
            Phone = student.Phone;
        }
    }
}
