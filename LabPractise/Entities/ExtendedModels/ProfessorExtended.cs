﻿using Entities.Intefaces;
using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.ExtendedModels
{
    public class ProfessorExtended : IEntity
    {
        public int ProfessorId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ProfessorRank { get; set; }

        public IEnumerable<GroupForLab> ProfessorGroupForLab { get; set; }
        public IEnumerable<Course> ProfessorCourse { get; set; }

        public ProfessorExtended(Professor professor)
        {
            ProfessorId = professor.ProfessorId;
            FirstName = professor.FirstName;
            LastName = professor.LastName;
            ProfessorRank = professor.ProfessorRank;
        }
    }
}
