﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class StudentTestResult
    {
        public int StudentTestResultId { get; set; }
        public int StudentId { get; set; }
        public int TestId { get; set; }
        public int ResultPercent { get; set; }
        public DateTime? DateResult { get; set; }

        public virtual Student Student { get; set; }
        public virtual Test Test { get; set; }
    }
}
