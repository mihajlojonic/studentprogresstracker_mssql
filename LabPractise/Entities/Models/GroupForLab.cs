﻿using Entities.Intefaces;
using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class GroupForLab : IEntity
    {
        public GroupForLab()
        {
            ProfessorGroup = new HashSet<ProfessorGroup>();
            Rating = new HashSet<Rating>();
            StudentGroup = new HashSet<StudentGroup>();
        }

        public int GroupForLabId { get; set; }
        public int? CourseId { get; set; }
        public string DescriptionGroup { get; set; }
        public byte NoGroup { get; set; }

        public virtual ICollection<ProfessorGroup> ProfessorGroup { get; set; }
        public virtual ICollection<Rating> Rating { get; set; }
        public virtual ICollection<Schedule> Schedule { get; set; }
        public virtual ICollection<StudentGroup> StudentGroup { get; set; }
        public virtual Course Course { get; set; }
    }
}
