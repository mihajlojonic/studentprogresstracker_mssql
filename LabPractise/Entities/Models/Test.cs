﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class Test
    {
        public Test()
        {
            Question = new HashSet<Question>();
            StudentTestResult = new HashSet<StudentTestResult>();
        }

        public int TestId { get; set; }
        public string TestName { get; set; }
        public string TestDescription { get; set; }
        public int? TestNum { get; set; }
        public int CourseId { get; set; }

        public virtual ICollection<Question> Question { get; set; }
        public virtual ICollection<StudentTestResult> StudentTestResult { get; set; }
        public virtual Course Course { get; set; }
    }
}
