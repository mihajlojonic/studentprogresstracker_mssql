﻿using Entities.Intefaces;
using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class Student : IEntity
    {
        public Student()
        {
            ProfessorRating = new HashSet<ProfessorRating>();
            Rating = new HashSet<Rating>();
            StudentCourse = new HashSet<StudentCourse>();
            StudentDepartment = new HashSet<StudentDepartment>();
            StudentGroup = new HashSet<StudentGroup>();
            StudentTestResult = new HashSet<StudentTestResult>();
        }

        public int StudentId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? BirthDate { get; set; }
        public string BirthPlace { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }

        public virtual ICollection<ProfessorRating> ProfessorRating { get; set; }
        public virtual ICollection<Rating> Rating { get; set; }
        public virtual ICollection<StudentCourse> StudentCourse { get; set; }
        public virtual ICollection<StudentDepartment> StudentDepartment { get; set; }
        public virtual ICollection<StudentGroup> StudentGroup { get; set; }
        public virtual ICollection<StudentTestResult> StudentTestResult { get; set; }
    }
}
