﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class LabPreparation
    {
        public int LabPreparationId { get; set; }
        public string LabPreparationName { get; set; }
        public string LabPreparationDescription { get; set; }
        public string Link { get; set; }
        public int CourseId { get; set; }

        public virtual Course Course { get; set; }
    }
}
