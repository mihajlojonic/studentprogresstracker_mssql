﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Entities.Models
{
    public partial class LabExeDBContext : DbContext
    {
        public virtual DbSet<Answer> Answer { get; set; }
        public virtual DbSet<Course> Course { get; set; }
        public virtual DbSet<Department> Department { get; set; }
        public virtual DbSet<GroupForLab> GroupForLab { get; set; }
        public virtual DbSet<LabPreparation> LabPreparation { get; set; }
        public virtual DbSet<LabTask> LabTask { get; set; }
        public virtual DbSet<Lecture> Lecture { get; set; }
        public virtual DbSet<Professor> Professor { get; set; }
        public virtual DbSet<ProfessorCourse> ProfessorCourse { get; set; }
        public virtual DbSet<ProfessorDepartment> ProfessorDepartment { get; set; }
        public virtual DbSet<ProfessorGroup> ProfessorGroup { get; set; }
        public virtual DbSet<ProfessorRating> ProfessorRating { get; set; }
        public virtual DbSet<Question> Question { get; set; }
        public virtual DbSet<Rating> Rating { get; set; }
        public virtual DbSet<Student> Student { get; set; }
        public virtual DbSet<StudentCourse> StudentCourse { get; set; }
        public virtual DbSet<StudentDepartment> StudentDepartment { get; set; }
        public virtual DbSet<StudentGroup> StudentGroup { get; set; }
        public virtual DbSet<StudentTestResult> StudentTestResult { get; set; }
        public virtual DbSet<Test> Test { get; set; }

        public LabExeDBContext(DbContextOptions options)
            : base(options)
        {
        }

        
        /*
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
            optionsBuilder.UseSqlServer(@"Server=DESKTOP-HE8G2S5\SQLEXPRESS;Database=LabPractiseDB;Trusted_Connection=True;");
        }*/

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Answer>(entity =>
            {
                entity.Property(e => e.AnswerValue)
                    .IsRequired()
                    .HasColumnType("varchar(256)");

                entity.HasOne(d => d.Question)
                    .WithMany(p => p.Answer)
                    .HasForeignKey(d => d.QuestionId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Answer_Question");
            });

            modelBuilder.Entity<Course>(entity =>
            {
                entity.Property(e => e.CourseName)
                    .IsRequired()
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.Description).HasColumnType("varchar(50)");

                entity.Property(e => e.Description2).HasColumnType("varchar(1000)");

                entity.Property(e => e.ShortName).HasColumnType("varchar(50)");

                entity.HasOne(d => d.Department)
                    .WithMany(p => p.Course)
                    .HasForeignKey(d => d.DepartmentId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Course_Department");
            });

            modelBuilder.Entity<Department>(entity =>
            {
                entity.Property(e => e.DepartmentName)
                    .IsRequired()
                    .HasColumnType("varchar(100)");
            });

            modelBuilder.Entity<GroupForLab>(entity =>
            {
                entity.Property(e => e.DescriptionGroup).HasColumnType("varchar(256)");

                entity.HasOne(d => d.Course)
                    .WithMany(p => p.GroupForLab)
                    .HasForeignKey(d => d.CourseId)
                    .HasConstraintName("FK_GroupForLab_Course");
            });

            modelBuilder.Entity<Schedule>(entity =>
            {
                entity.Property(e => e.DateTimeStart).HasColumnType("datetime");
                entity.Property(e => e.Room).HasColumnType("varchar(100)");

                entity.HasOne(g => g.GroupForLab)
                      .WithMany(s => s.Schedule)
                      .HasForeignKey(g => g.GroupForLabId)
                      .HasConstraintName("FK_Schedule_GroupForLab");
            });

            modelBuilder.Entity<LabPreparation>(entity =>
            {
                entity.Property(e => e.LabPreparationDescription).HasColumnType("varchar(256)");

                entity.Property(e => e.LabPreparationName)
                    .IsRequired()
                    .HasColumnType("varchar(256)");

                entity.Property(e => e.Link).HasColumnType("varchar(256)");

                entity.HasOne(d => d.Course)
                    .WithMany(p => p.LabPreparation)
                    .HasForeignKey(d => d.CourseId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_LabPreparation_Course");
            });

            modelBuilder.Entity<LabTask>(entity =>
            {
                entity.Property(e => e.LabTaskDescription).HasColumnType("varchar(256)");

                entity.Property(e => e.LabTaskName)
                    .IsRequired()
                    .HasColumnType("varchar(256)");

                entity.Property(e => e.Link).HasColumnType("varchar(256)");

                entity.HasOne(d => d.Course)
                    .WithMany(p => p.LabTask)
                    .HasForeignKey(d => d.CourseId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_LabTask_Course");
            });

            modelBuilder.Entity<Lecture>(entity =>
            {
                entity.Property(e => e.LectureDescription).HasColumnType("varchar(256)");

                entity.Property(e => e.LectureName)
                    .IsRequired()
                    .HasColumnType("varchar(256)");

                entity.Property(e => e.Link).HasColumnType("varchar(256)");

                entity.HasOne(d => d.Course)
                    .WithMany(p => p.Lecture)
                    .HasForeignKey(d => d.CourseId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Lecture_Course");
            });

            modelBuilder.Entity<Professor>(entity =>
            {
                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.ProfessorRank)
                    .IsRequired()
                    .HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<ProfessorCourse>(entity =>
            {
                entity.HasOne(d => d.Course)
                    .WithMany(p => p.ProfessorCourse)
                    .HasForeignKey(d => d.CourseId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_ProfessorCourse_Course");

                entity.HasOne(d => d.Professor)
                    .WithMany(p => p.ProfessorCourse)
                    .HasForeignKey(d => d.ProfessorId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Professor_Course_Professor");
            });

            modelBuilder.Entity<ProfessorDepartment>(entity =>
            {
                entity.HasOne(d => d.Department)
                    .WithMany(p => p.ProfessorDepartment)
                    .HasForeignKey(d => d.DepartmentId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_ProfessorDepartment_Department");

                entity.HasOne(d => d.Professor)
                    .WithMany(p => p.ProfessorDepartment)
                    .HasForeignKey(d => d.ProfessorId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_ProfessorDepartment_Professor");
            });

            modelBuilder.Entity<ProfessorGroup>(entity =>
            {
                entity.HasOne(d => d.GroupForLab)
                    .WithMany(p => p.ProfessorGroup)
                    .HasForeignKey(d => d.GroupForLabId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_ProfessorGroup_GroupForLab");

                entity.HasOne(d => d.Professor)
                    .WithMany(p => p.ProfessorGroup)
                    .HasForeignKey(d => d.ProfessorId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_ProfessorGroup_Professor");
            });

            modelBuilder.Entity<ProfessorRating>(entity =>
            {
                entity.Property(e => e.DateRating).HasColumnType("datetime");

                entity.HasOne(d => d.Professor)
                    .WithMany(p => p.ProfessorRating)
                    .HasForeignKey(d => d.ProfessorId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_ProfessorRating_Professor");

                entity.HasOne(d => d.Student)
                    .WithMany(p => p.ProfessorRating)
                    .HasForeignKey(d => d.StudentId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_ProfessorRating_Student");
            });

            modelBuilder.Entity<Question>(entity =>
            {
                entity.Property(e => e.QuestionText)
                    .IsRequired()
                    .HasColumnType("varchar(512)");

                entity.Property(e => e.QuestionType)
                    .IsRequired()
                    .HasColumnType("varchar(32)");

                entity.HasOne(d => d.Test)
                    .WithMany(p => p.Question)
                    .HasForeignKey(d => d.TestId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Question_Test");
            });

            modelBuilder.Entity<Rating>(entity =>
            {
                entity.Property(e => e.Comment).HasColumnType("varchar(256)");

                entity.HasOne(d => d.Course)
                    .WithMany(p => p.Rating)
                    .HasForeignKey(d => d.CourseId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Rating_Course");

                entity.HasOne(d => d.GroupForLab)
                    .WithMany(p => p.Rating)
                    .HasForeignKey(d => d.GroupForLabId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Rating_GroupForLab");

                entity.HasOne(d => d.Professor)
                    .WithMany(p => p.Rating)
                    .HasForeignKey(d => d.ProfessorId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Rating_Professor");

                entity.HasOne(d => d.Student)
                    .WithMany(p => p.Rating)
                    .HasForeignKey(d => d.StudentId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Rating_Student");

                entity.HasOne(d => d.Schedule)
                .WithMany(p => p.Rating)
                .HasForeignKey(d => d.ScheduleId)
                .OnDelete(DeleteBehavior.Restrict)
                .HasConstraintName("FK_Rating_Schedule");
            });

            modelBuilder.Entity<Student>(entity =>
            {
                entity.Property(e => e.Address).HasColumnType("varchar(50)");

                entity.Property(e => e.BirthDate).HasColumnType("smalldatetime");

                entity.Property(e => e.BirthPlace).HasColumnType("varchar(50)");

                entity.Property(e => e.FirstName).HasColumnType("varchar(50)");

                entity.Property(e => e.LastName).HasColumnType("varchar(50)");

                entity.Property(e => e.Phone).HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<StudentCourse>(entity =>
            {
                entity.HasOne(d => d.Course)
                    .WithMany(p => p.StudentCourse)
                    .HasForeignKey(d => d.CourseId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Course");

                entity.HasOne(d => d.Student)
                    .WithMany(p => p.StudentCourse)
                    .HasForeignKey(d => d.StudentId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Student");
            });

            modelBuilder.Entity<StudentDepartment>(entity =>
            {
                entity.HasOne(d => d.Department)
                    .WithMany(p => p.StudentDepartment)
                    .HasForeignKey(d => d.DepartmentId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_StudentDepartment_Department");

                entity.HasOne(d => d.Student)
                    .WithMany(p => p.StudentDepartment)
                    .HasForeignKey(d => d.StudentId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_StudentDepartment_Student");
            });

            modelBuilder.Entity<StudentGroup>(entity =>
            {
                entity.HasOne(d => d.GroupForLab)
                    .WithMany(p => p.StudentGroup)
                    .HasForeignKey(d => d.GroupForLabId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_StudentGroup_GroupForLab");

                entity.HasOne(d => d.Student)
                    .WithMany(p => p.StudentGroup)
                    .HasForeignKey(d => d.StudentId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_StudentGroup_Student");
            });

            modelBuilder.Entity<StudentTestResult>(entity =>
            {
                entity.Property(e => e.DateResult).HasColumnType("datetime");

                entity.HasOne(d => d.Student)
                    .WithMany(p => p.StudentTestResult)
                    .HasForeignKey(d => d.StudentId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_StudentTestResult_Student");

                entity.HasOne(d => d.Test)
                    .WithMany(p => p.StudentTestResult)
                    .HasForeignKey(d => d.TestId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_StudentTestResult_Test");
            });

            modelBuilder.Entity<Test>(entity =>
            {
                entity.Property(e => e.TestDescription).HasColumnType("varchar(256)");

                entity.Property(e => e.TestName)
                    .IsRequired()
                    .HasColumnType("varchar(256)");

                entity.HasOne(d => d.Course)
                    .WithMany(p => p.Test)
                    .HasForeignKey(d => d.CourseId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Test_Course");
            });
        }
    }
}