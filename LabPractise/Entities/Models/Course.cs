﻿using Entities.Intefaces;
using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class Course : IEntity
    {
        public Course()
        {
            GroupForLab = new HashSet<GroupForLab>();
            LabPreparation = new HashSet<LabPreparation>();
            LabTask = new HashSet<LabTask>();
            Lecture = new HashSet<Lecture>();
            ProfessorCourse = new HashSet<ProfessorCourse>();
            Rating = new HashSet<Rating>();
            StudentCourse = new HashSet<StudentCourse>();
            Test = new HashSet<Test>();
        }

        public int CourseId { get; set; }
        public string CourseName { get; set; }
        public string ShortName { get; set; }
        public int Year { get; set; }
        public string Description { get; set; }
        public int DepartmentId { get; set; }
        public string Description2 { get; set; }

        public virtual ICollection<GroupForLab> GroupForLab { get; set; }
        public virtual ICollection<LabPreparation> LabPreparation { get; set; }
        public virtual ICollection<LabTask> LabTask { get; set; }
        public virtual ICollection<Lecture> Lecture { get; set; }
        public virtual ICollection<ProfessorCourse> ProfessorCourse { get; set; }
        public virtual ICollection<Rating> Rating { get; set; }
        public virtual ICollection<StudentCourse> StudentCourse { get; set; }
        public virtual ICollection<Test> Test { get; set; }
        public virtual Department Department { get; set; }
    }
}
