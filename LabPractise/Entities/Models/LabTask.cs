﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class LabTask
    {
        public int LabTaskId { get; set; }
        public string LabTaskName { get; set; }
        public string LabTaskDescription { get; set; }
        public string Link { get; set; }
        public int CourseId { get; set; }

        public virtual Course Course { get; set; }
    }
}
