﻿using Entities.Intefaces;
using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class Professor : IEntity
    {
        public Professor()
        {
            ProfessorCourse = new HashSet<ProfessorCourse>();
            ProfessorDepartment = new HashSet<ProfessorDepartment>();
            ProfessorGroup = new HashSet<ProfessorGroup>();
            ProfessorRating = new HashSet<ProfessorRating>();
            Rating = new HashSet<Rating>();
        }

        public int ProfessorId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ProfessorRank { get; set; }

        public virtual ICollection<ProfessorCourse> ProfessorCourse { get; set; }
        public virtual ICollection<ProfessorDepartment> ProfessorDepartment { get; set; }
        public virtual ICollection<ProfessorGroup> ProfessorGroup { get; set; }
        public virtual ICollection<ProfessorRating> ProfessorRating { get; set; }
        public virtual ICollection<Rating> Rating { get; set; }
    }
}
