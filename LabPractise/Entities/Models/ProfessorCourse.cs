﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class ProfessorCourse
    {
        public int ProfessorCourseId { get; set; }
        public int ProfessorId { get; set; }
        public int CourseId { get; set; }

        public virtual Course Course { get; set; }
        public virtual Professor Professor { get; set; }
    }
}
