﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class Answer
    {
        public int AnswerId { get; set; }
        public string AnswerValue { get; set; }
        public int IsTrueAnswer { get; set; }
        public int QuestionId { get; set; }

        public virtual Question Question { get; set; }
    }
}
