﻿using Entities.Intefaces;
using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class ProfessorGroup : IEntity
    {
        public int ProfessorGroupId { get; set; }
        public int ProfessorId { get; set; }
        public int GroupForLabId { get; set; }

        public virtual GroupForLab GroupForLab { get; set; }
        public virtual Professor Professor { get; set; }
    }
}
