﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class Rating
    {
        public int RatingId { get; set; }
        public int StudentId { get; set; }
        public int ProfessorId { get; set; }
        public int CourseId { get; set; }
        public int GroupForLabId { get; set; }
        public int ScheduleId { get; set; }// //////
        public int Rate { get; set; }
        public string Comment { get; set; }
        
        public virtual Course Course { get; set; }
        public virtual GroupForLab GroupForLab { get; set; }
        public virtual Professor Professor { get; set; }
        public virtual Student Student { get; set; }
        public virtual Schedule Schedule { get; set; }
    }
}
