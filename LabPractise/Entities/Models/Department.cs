﻿using Entities.Intefaces;
using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class Department : IEntity
    {
        public Department()
        {
            Course = new HashSet<Course>();
            ProfessorDepartment = new HashSet<ProfessorDepartment>();
            StudentDepartment = new HashSet<StudentDepartment>();
        }

        public int DepartmentId { get; set; }
        public string DepartmentName { get; set; }

        public virtual ICollection<Course> Course { get; set; }
        public virtual ICollection<ProfessorDepartment> ProfessorDepartment { get; set; }
        public virtual ICollection<StudentDepartment> StudentDepartment { get; set; }
    }
}
