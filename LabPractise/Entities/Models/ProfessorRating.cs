﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class ProfessorRating
    {
        public int ProfessorRatingId { get; set; }
        public int StudentId { get; set; }
        public int ProfessorId { get; set; }
        public int Rate { get; set; }
        public int VersionRate { get; set; }
        public DateTime? DateRating { get; set; }

        public virtual Professor Professor { get; set; }
        public virtual Student Student { get; set; }
    }
}
