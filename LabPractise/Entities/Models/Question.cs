﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class Question
    {
        public Question()
        {
            Answer = new HashSet<Answer>();
        }

        public int QuestionId { get; set; }
        public string QuestionText { get; set; }
        public string QuestionType { get; set; }
        public int? QuestionWeight { get; set; }
        public int TestId { get; set; }

        public virtual ICollection<Answer> Answer { get; set; }
        public virtual Test Test { get; set; }
    }
}
