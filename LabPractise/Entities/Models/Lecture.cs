﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class Lecture
    {
        public int LectureId { get; set; }
        public string LectureName { get; set; }
        public string LectureDescription { get; set; }
        public string Link { get; set; }
        public int CourseId { get; set; }

        public virtual Course Course { get; set; }
    }
}
