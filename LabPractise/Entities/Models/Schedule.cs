﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Models
{
    public partial class Schedule
    {
        public int ScheduleId { get; set; }
        public int GroupForLabId { get; set; }
        public int NumLab { get; set; }
        public DateTime DateTimeStart { get; set; }
        public int DurationMinutes { get; set; }
        public string Room { get; set; }

        public virtual GroupForLab GroupForLab { get; set; }

        public virtual ICollection<Rating> Rating { get; set; }
    }
}
