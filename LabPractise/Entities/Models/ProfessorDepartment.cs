﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class ProfessorDepartment
    {
        public int ProfessorDepartmentId { get; set; }
        public int ProfessorId { get; set; }
        public int DepartmentId { get; set; }

        public virtual Department Department { get; set; }
        public virtual Professor Professor { get; set; }
    }
}
