﻿using Entities.Intefaces;
using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class StudentGroup : IEntity
    {
        public int StudentGroupId { get; set; }
        public int StudentId { get; set; }
        public int GroupForLabId { get; set; }

        public virtual GroupForLab GroupForLab { get; set; }
        public virtual Student Student { get; set; }
    }
}
