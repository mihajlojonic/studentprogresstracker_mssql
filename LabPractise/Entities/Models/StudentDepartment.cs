﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class StudentDepartment
    {
        public int StudentDepartmentId { get; set; }
        public int StudentId { get; set; }
        public int DepartmentId { get; set; }

        public virtual Department Department { get; set; }
        public virtual Student Student { get; set; }
    }
}
