﻿using Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Entities.EntityFrameworkExtensions
{
    public static class StoredProcedureExtension
    {
        
        public static DbCommand LoadStoredProcedure(this LabExeDBContext context, string nameStoredProcedure)
        {

            var cmd = context.Database.GetDbConnection().CreateCommand();
            cmd.CommandText = nameStoredProcedure;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;

            return cmd;
        }
        
        public static DbCommand WithParam(this DbCommand cmd, string namePrameter, object value)
        {
            if (string.IsNullOrEmpty(cmd.CommandText) && cmd.CommandType != System.Data.CommandType.StoredProcedure)
                throw new InvalidOperationException("StoredProcedure hasn't loaded");

            var param = cmd.CreateParameter();
            param.ParameterName = namePrameter;
            param.Value = (value != null ? value : DBNull.Value);
            cmd.Parameters.Add(param);
            return cmd;
        }

            //todo: generic, mapping results
        public static void ExecuteStoredProcedure(this DbCommand command)
        {
            using (command)
            {
                if (command.Connection.State == System.Data.ConnectionState.Closed)
                    command.Connection.Open();
                try
                {
                    
                    var reader = command.ExecuteReader();
                }
                finally
                {
                    command.Connection.Close();
                }
            }
        }
    }
}
