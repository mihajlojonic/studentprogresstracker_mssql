﻿using Contracts;
using Entities.EntityFrameworkExtensions;
using Entities.Extensions;
using Entities.Models;
using Entities.StoredProceduresUpdateModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class ProfessorGroupRepository : RepositoryBase<ProfessorGroup>, IProfessorGroupRepository
    {
        public ProfessorGroupRepository(LabExeDBContext dbc) : base(dbc)
        {
        }

        public async Task<IEnumerable<ProfessorGroup>> GetAllProfessorGroupAsync()
        {
            var professorGroups = await FindAllAsync();
            return professorGroups;
        }

        public async Task<ProfessorGroup> GetProfessorGroupByIdAsync(int professorGroupId)
        {
            var professorGroup = await FindByConditionAync(pg => pg.ProfessorGroupId.Equals(professorGroupId));
            return professorGroup.DefaultIfEmpty(new ProfessorGroup()).FirstOrDefault();
        }

        public async Task SendToProfessorGroup(ProfessorGroupSendTo professorGroupSendTo)
        {
            context.LoadStoredProcedure("SendToProfessorsGroups")
                .WithParam("@ProfessorId", professorGroupSendTo.ProfessorId)
                .WithParam("@ProfessorNewGroupId", professorGroupSendTo.ProfessorNewGroupId)
                .WithParam("@ProfessorPrevGroupId", professorGroupSendTo.ProfessorPrevGroupId)
                .ExecuteStoredProcedure();
        }

        public async Task CreateProfessorGroupAsync(ProfessorGroup professorGroup)
        {
            Create(professorGroup);
            await SaveAsync();
        }

        public async Task UpdateProfessorGroupAsync(ProfessorGroup dbProfessorGroup, ProfessorGroup professorGroup)
        {
            dbProfessorGroup.Map(professorGroup);
            Update(dbProfessorGroup);
            await SaveAsync();
        }

        public async Task DeleteProfessorGroupAsync(ProfessorGroup professorGroup)
        {
            Delete(professorGroup);
            await SaveAsync();
        }
    }
}
