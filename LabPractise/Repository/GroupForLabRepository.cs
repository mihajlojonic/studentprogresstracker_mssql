﻿using Contracts;
using Entities.ExtendedModels;
using Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class GroupForLabRepository : RepositoryBase<GroupForLab>, IGroupForLabRepository
    {
        public GroupForLabRepository(LabExeDBContext context) : base(context)
        {
        }

        public async Task<IEnumerable<GroupForLab>> GetAllGroupsForLabAsync()
        {
            var groups = await FindAllAsync();
            return groups;
        }

        public async Task<GroupForLab> GetGroupForLabByIdAsync(int groupId)
        {
            var group = await FindByConditionAync(g => g.GroupForLabId.Equals(groupId));
            return group.DefaultIfEmpty(new GroupForLab()).FirstOrDefault();
        }

        public async Task<IEnumerable<GroupForLab>> GetGroupForLabByCourseIdAsync(int courseId)
        {
            var groups = await FindByConditionAync(g => g.CourseId.Equals(courseId));
            return groups;
        }

        public async Task<IEnumerable<GroupForLabExtended>> GetGroupForLabFullAsync(int courseId)
        {
            List<GroupForLabExtended> returnList = new List<GroupForLabExtended>();
            var groups = await GetGroupForLabByCourseIdAsync(courseId);
            
            foreach(GroupForLab gfl in  groups)
            {
                var queryStudents = from student in context.Student
                                    where student.StudentGroup.Any(s => s.GroupForLabId == gfl.GroupForLabId)
                                    select student;

                var queryProfessor = from professor in context.Professor
                                     where professor.ProfessorGroup.Any(p => p.GroupForLabId == gfl.GroupForLabId)
                                     select professor;

                returnList.Add(new GroupForLabExtended(gfl)
                {
                    ProfessorsForGroup = await queryProfessor.ToListAsync(),
                    StudentsForGroup = await queryStudents.ToListAsync()
                });
            }

            return returnList;
        }
    }
}
