﻿using Contracts;
using Entities.EntityFrameworkExtensions;
using Entities.Extensions;
using Entities.Models;
using Entities.StoredProceduresUpdateModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class StudentGroupRepository : RepositoryBase<StudentGroup>, IStudentGroupRepository
    {
        public StudentGroupRepository(LabExeDBContext dbc) : base(dbc)
        {}

        public async Task<IEnumerable<StudentGroup>> GetAllStudentGroupAsync()
        {
            var studentsGroups = await FindAllAsync();
            return studentsGroups;
        }

        public async Task<StudentGroup> GetStudentGroupByIdAsync(int studentGroupId)
        {
            var studentGroup = await FindByConditionAync(sg => sg.StudentGroupId.Equals(studentGroupId));
            return studentGroup.DefaultIfEmpty(new StudentGroup()).FirstOrDefault();
        }

        public async Task SwapStudentsGroup(StudentGroupSwap studentGroupSwap)
        {
            /*
            using (var conn = context.Database.GetDbConnection())
            {
                conn.Open();

                var cmd = context.Database.GetDbConnection().CreateCommand();
                cmd.CommandText = "SwapStudentsGroups";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;

                var param1 = cmd.CreateParameter();
                param1.ParameterName = "@Student1_StudentId";
                param1.Value = studentGroupSwap.Student1_StudentId;
                cmd.Parameters.Add(param1);

                var param2 = cmd.CreateParameter();
                param2.ParameterName = "@Student1_GroupForLabId";
                param2.Value = studentGroupSwap.Student1_GroupForLabId;
                cmd.Parameters.Add(param2);

                var param3 = cmd.CreateParameter();
                param3.ParameterName = "@Student2_StudentId";
                param3.Value = studentGroupSwap.Student2_StudentId;
                cmd.Parameters.Add(param3);

                var param4 = cmd.CreateParameter();
                param4.ParameterName = "@Student2_GroupForLabId";
                param4.Value = studentGroupSwap.Student2_GroupForLabId;
                cmd.Parameters.Add(param4);

                cmd.ExecuteReader();

                conn.Close();
            }*/

            context.LoadStoredProcedure("SwapStudentsGroups")
                .WithParam("@Student1_StudentId", studentGroupSwap.Student1_StudentId)
                .WithParam("@Student1_GroupForLabId", studentGroupSwap.Student1_GroupForLabId)
                .WithParam("@Student2_StudentId", studentGroupSwap.Student2_StudentId)
                .WithParam("@Student2_GroupForLabId", studentGroupSwap.Student2_GroupForLabId)
                .ExecuteStoredProcedure();
            
            /*
            var paramStudent1_StudentId = new SqlParameter("@Student1_StudentId", studentGroupSwap.Student1_StudentId);
            var paramStudent1_GroupForLabId = new SqlParameter("@Student1_GroupForLabId", studentGroupSwap.Student1_GroupForLabId);
            var paramStudent2_StudentId = new SqlParameter("@Student2_StudentId", studentGroupSwap.Student2_StudentId);
            var paramStudent2_GroupForLabId = new SqlParameter("@Student2_GroupForLabId", studentGroupSwap.Student2_GroupForLabId);
            
            var commandText = $"EXEC	SwapStudentsGroups @Student1_StudentId, @Student1_GroupForLabId, @Student2_StudentId, @Student2_GroupForLabId";
            context.Database.ExecuteSqlCommand(commandText, paramStudent1_StudentId, paramStudent1_GroupForLabId, paramStudent2_StudentId, paramStudent2_GroupForLabId);*/
        }

        public async Task SendToStudentGroup(StudentGroupSendTo studentGroupSendTo)
        {
            context.LoadStoredProcedure("SendToStudentsGroups")
                .WithParam("@StudentId", studentGroupSendTo.StudentId)
                .WithParam("@StudentNewGroupId", studentGroupSendTo.StudentNewGroupId)
                .WithParam("@StudentPrevGroupId", studentGroupSendTo.StudentPrevGroupId)
                .ExecuteStoredProcedure();
        }

        public async Task CreateStudentGroupAsync(StudentGroup studentGroup)
        {
            Create(studentGroup);
            await SaveAsync();
        }

        public async Task UpdateStudentGroupAsync(StudentGroup dbStudentGroup, StudentGroup studentGroup)
        {
            dbStudentGroup.Map(studentGroup);
            Update(dbStudentGroup);
            await SaveAsync();
        }

        public async Task DeleteStudentGroupAsync(StudentGroup studentGroup)
        {
            Delete(studentGroup);
            await SaveAsync();
        }
    }
}
