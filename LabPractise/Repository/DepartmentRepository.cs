﻿using Contracts;
using Entities.Extensions;
using Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class DepartmentRepository : RepositoryBase<Department>, IDepartmentRepository
    {
        public DepartmentRepository(LabExeDBContext dbc) : base(dbc)
        {
        }

        public async Task<IEnumerable<Department>> GetAllDepartmentsAsync()
        {
            var departments = await FindAllAsync();
            return departments.OrderBy(d => d.DepartmentName);
        }

        public async Task<Department> GetDepartmentById(int id)
        {
            var department = await FindByConditionAync(d => d.DepartmentId.Equals(id));
            return department.DefaultIfEmpty(new Department()).FirstOrDefault();
        }

        public async Task CreateDepartmentAsync(Department department)
        {
            Create(department);
            await SaveAsync();
        }

        public async Task UpdateDepartmentAsync(Department dbDepartment, Department department)
        {
            dbDepartment.Map(department);
            Update(dbDepartment);
            await SaveAsync();
        }

        public async Task DeleteDepartmnetAsync(Department department)
        {
            Delete(department);
            await SaveAsync();
        }
    }
}
