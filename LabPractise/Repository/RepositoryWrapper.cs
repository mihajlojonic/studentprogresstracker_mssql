﻿using Contracts;
using Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Repository
{
    public class RepositoryWrapper : IRepositoryWrapper
    {
        private LabExeDBContext _repoContext;
        private ICourseRepository _repoCourse;
        private IGroupForLabRepository _repoGroupForLab;
        private IDepartmentRepository _repoDepartment;
        private IProfessorRepository _repoProfessor;
        private IStudentRepository _repoStudent;
        private IStudentGroupRepository _repoStudentGroup;
        private IProfessorGroupRepository _repoProfessorGroup;

        public ICourseRepository Course
        {
            get
            {
                if (_repoCourse == null)
                {
                    _repoCourse = new CourseRepository(_repoContext);
                }

                return _repoCourse;
            }
        }

        public IGroupForLabRepository GroupForLab
        {
            get
            {
                if (_repoGroupForLab == null)
                {
                    _repoGroupForLab = new GroupForLabRepository(_repoContext);
                }

                return _repoGroupForLab;
            }
        }

        public IDepartmentRepository Department
        {
            get
            {
                if(_repoDepartment == null)
                {
                    _repoDepartment = new DepartmentRepository(_repoContext);
                }

                return _repoDepartment;
            }
        }

        public IProfessorRepository Professor
        {
            get
            {
                if(_repoProfessor == null)
                {
                    _repoProfessor = new ProfessorRepository(_repoContext);
                }

                return _repoProfessor;
            }
        }

        public IStudentRepository Student
        {
            get
            {
                if(_repoStudent == null)
                {
                    _repoStudent = new StudentRepository(_repoContext);
                }

                return _repoStudent;
            }
        }

        public IStudentGroupRepository StudentGroup
        {
            get
            {
                if(_repoStudentGroup == null)
                {
                    _repoStudentGroup = new StudentGroupRepository(_repoContext);
                }

                return _repoStudentGroup;
            }
        }

        public IProfessorGroupRepository ProfessorGroup
        {
            get
            {
                if (_repoProfessorGroup == null)
                {
                    _repoProfessorGroup = new ProfessorGroupRepository(_repoContext);
                }

                return _repoProfessorGroup;
            }
        }


        public RepositoryWrapper(LabExeDBContext context)
        {
            _repoContext = context;
        }
    }
}
