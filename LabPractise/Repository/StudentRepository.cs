﻿using Contracts;
using Entities.ExtendedModels;
using Entities.Extensions;
using Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class StudentRepository : RepositoryBase<Student>, IStudentRepository
    {
        public StudentRepository(LabExeDBContext dbc) : base(dbc)
        {}

        public async Task<IEnumerable<Student>> GetAllStudentsAsync()
        {
            var students = await FindAllAsync();
            return students.OrderBy(s => s.FirstName);
        }

        public async Task<Student> GetStudentByIdAsync(int studentId)
        {
            var student = await FindByConditionAync(s => s.StudentId.Equals(studentId));
            return student.DefaultIfEmpty(new Student()).FirstOrDefault();
        }

        public async Task<StudentExtended> GetStudentGroupForLabAsync(int studentId)
        {
            var student = await GetStudentByIdAsync(studentId);
            var query = from groupLab in context.GroupForLab
                        where groupLab.StudentGroup.Any(g => g.StudentId == studentId)
                        select groupLab;

            return new StudentExtended(student)
            {
                StudentGroupForLab = await query.ToListAsync()
            };
        }

        public async Task<StudentExtended> GetStudentCourseAsync(int studentId)
        {
            var student = await GetStudentByIdAsync(studentId);
            var query = from course in context.Course
                        where course.StudentCourse.Any(c => c.StudentId == studentId)
                        select course;

            return new StudentExtended(student)
            {
                StudentCourse = await query.ToListAsync()
            };
        }

        public async Task CreateStudentAsync(Student student)
        {
            Create(student);
            await SaveAsync();
        }

        public async Task UpdateStudentAsync(Student dbStudent, Student student)
        {
            dbStudent.Map(student);
            Update(dbStudent);
            await SaveAsync();
        }

        public async Task DeleteStudentAsync(Student student)
        {
            Delete(student);
            await SaveAsync();
        }
    }
}
