﻿using Contracts;
using Entities.ExtendedModels;
using Entities.Extensions;
using Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class ProfessorRepository : RepositoryBase<Professor>, IProfessorRepository
    {
        public ProfessorRepository(LabExeDBContext dbc) : base(dbc)
        {}

        public async Task<IEnumerable<Professor>> GetAllProfessorsAsync()
        {
            var professors = await FindAllAsync();
            return professors.OrderBy(p => p.FirstName);
        }

        public async Task<Professor> GetProfessorByIdAsync(int professorId)
        {
            var professor = await FindByConditionAync(p => p.ProfessorId.Equals(professorId));
            return professor.DefaultIfEmpty(new Professor()).FirstOrDefault();
        }

        public async Task<ProfessorExtended> GetProfessorGroupForLabAsync(int professorId)
        {
            var professor = await GetProfessorByIdAsync(professorId);
            var query = from grouplab in context.GroupForLab
                        where grouplab.ProfessorGroup.Any(g => g.ProfessorId == professorId)
                        select grouplab;

            return new ProfessorExtended(professor)
            {
                ProfessorGroupForLab = await query.ToListAsync()
            };
        }

        public async Task<ProfessorExtended> GetProfessorCourseAsync(int professorId)
        {
            var professor = await GetProfessorByIdAsync(professorId);
            var query = from course in context.Course
                        where course.ProfessorCourse.Any(c => c.ProfessorId == professorId)
                        select course;

            return new ProfessorExtended(professor)
            {
                ProfessorCourse = await query.ToListAsync()
            };
        }
        
        public async Task CreateProfessorAsync(Professor professor)
        {
            Create(professor);
            await SaveAsync();
        }

        public async Task UpdateProfessorAsync(Professor dbProfessor, Professor professor)
        {
            dbProfessor.Map(professor);
            Update(dbProfessor);
            await SaveAsync();
        }

        public async Task DeleteProfessorAsync(Professor professor)
        {
            Delete(professor);
            await SaveAsync();
        }
    }
}
