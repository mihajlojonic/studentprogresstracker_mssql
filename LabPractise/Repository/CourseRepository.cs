﻿using Contracts;
using Entities.ExtendedModels;
using Entities.Extensions;
using Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class CourseRepository : RepositoryBase<Course>, ICourseRepository
    {
        public CourseRepository(LabExeDBContext context) : base(context)
        {
        }

        public async Task<IEnumerable<Course>> GetAllCoursesAsync()
        {
            var courses = await FindAllAsync();
            return courses.OrderBy(c => c.CourseName);
        }

        public async Task<Course> GetCourseByIdAsync(int courseId)
        {
            var course = await FindByConditionAync(c => c.CourseId.Equals(courseId));
            return course.DefaultIfEmpty(new Course()).FirstOrDefault();
        }

        public async Task<CourseExtended> GetCourseStudentsAsync(int courseId)
        {
            var course = await GetCourseByIdAsync(courseId);
            var query = from student in context.Student
                        where student.StudentCourse.Any(c => c.CourseId == courseId)
                        select student;

            return new CourseExtended(course)
            {
                StudentsOnCourse = await query.ToListAsync()
            };
        }

        public async Task<CourseExtended> GetCourseProfessorsAsync(int courseId)
        {
            var course = await GetCourseByIdAsync(courseId);
            var query = from professor in context.Professor
                        where professor.ProfessorCourse.Any(c => c.CourseId == courseId)
                        select professor;
            return new CourseExtended(course)
            {
                ProfessorsOnCourse = await query.ToListAsync()//context.Professor.Where(p => p.ProfessorCourse.All(c => c.CourseId == courseId))
            };
        }
        
        public async Task<CourseExtended> GetCourseFullAsync(int courseId)
        {
            var course = await GetCourseByIdAsync(courseId);

            var queryStudents = from student in context.Student
                                where student.StudentCourse.Any(c => c.CourseId == courseId)
                                select student;

            var queryProfessor = from professor in context.Professor
                                 where professor.ProfessorCourse.Any(c => c.CourseId == courseId)
                                 select professor;

            return new CourseExtended(course)
            {
                StudentsOnCourse = await queryStudents.ToListAsync(),//context.Student.Where(s => s.StudentCourse.All(c => c.CourseId == courseId)),
                ProfessorsOnCourse = await queryProfessor.ToListAsync()//context.Professor.Where(p => p.ProfessorCourse.All(c => c.CourseId == courseId)),
            };
        }

        public async Task CreateCourseAsync(Course course)
        {
            Create(course);
            await SaveAsync();
        }
        
        public async Task UpdateCourseAsync(Course dbCourse, Course Course)
        {
            dbCourse.Map(Course);
            Update(dbCourse);
            await SaveAsync();
        }

        public async Task DeleteCourseAsync(Course course)
        {
            Delete(course);
            await SaveAsync();
        }

        
    }
}
