use LabPractiseDB;
GO

CREATE PROCEDURE dbo.SwapStudentsGroups
(
	@Student1_StudentId int,
	@Student1_GroupForLabId int,
	@Student2_StudentId int,
	@Student2_GroupForLabId int
)
AS
BEGIN
    BEGIN TRANSACTION;
    SAVE TRANSACTION MySavePoint;

    BEGIN TRY
	UPDATE StudentGroup
	SET GroupForLabId = CASE WHEN StudentId = @Student1_StudentId AND GroupForLabId = @Student1_GroupForLabId THEN @Student2_GroupForLabId
							 WHEN StudentId = @Student2_StudentId AND GroupForLabId = @Student2_GroupForLabId  THEN @Student1_GroupForLabId
						END
						WHERE GroupForLabId IN (@Student1_GroupForLabId, @Student2_GroupForLabId) AND StudentId IN (@Student1_StudentId, @Student2_StudentId);
    END TRY

    BEGIN CATCH
        IF @@TRANCOUNT > 0
        BEGIN
            ROLLBACK TRANSACTION MySavePoint; -- rollback to MySavePoint
        END
    END CATCH
    
	COMMIT TRANSACTION 
END;

GO

CREATE PROCEDURE dbo.SendToStudentsGroups
(
	@StudentId int,
	@StudentNewGroupId int,
	@StudentPrevGroupId int
)
AS
BEGIN
    BEGIN TRANSACTION;
    SAVE TRANSACTION MySavePoint;

    BEGIN TRY
	
	UPDATE StudentGroup
	SET StudentGroup.GroupForLabId = @StudentNewGroupId
		WHERE StudentGroup.GroupForLabId = @StudentPrevGroupId AND StudentGroup.StudentId = @StudentId;
    END TRY

    BEGIN CATCH
        IF @@TRANCOUNT > 0
        BEGIN
            ROLLBACK TRANSACTION MySavePoint; -- rollback to MySavePoint
        END
    END CATCH
    
	COMMIT TRANSACTION 
END;

GO

CREATE PROCEDURE dbo.SendToProfessorsGroups
(
	@ProfessorId int,
	@ProfessorNewGroupId int,
	@ProfessorPrevGroupId int
)
AS
BEGIN
    BEGIN TRANSACTION;
    SAVE TRANSACTION MySavePoint;

    BEGIN TRY
	
	UPDATE ProfessorGroup
	SET ProfessorGroup.GroupForLabId = @ProfessorNewGroupId
		WHERE ProfessorGroup.GroupForLabId = @ProfessorPrevGroupId AND ProfessorGroup.ProfessorId = @ProfessorId;
    END TRY

    BEGIN CATCH
        IF @@TRANCOUNT > 0
        BEGIN
            ROLLBACK TRANSACTION MySavePoint; -- rollback to MySavePoint
        END
    END CATCH
    
	COMMIT TRANSACTION 
END;
*/