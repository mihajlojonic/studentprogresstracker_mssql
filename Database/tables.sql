CREATE DATABASE [LabPractiseDB] ON  PRIMARY 
( NAME = N'LabPractiseDB', FILENAME = N'C:\Program Files (x86)\Microsoft SQL Server\MSSQL.1\MSSQL\DATA\LabPractiseDB.mdf' , SIZE = 2048KB , FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'LabPractiseDB_log', FILENAME = N'C:\Program Files (x86)\Microsoft SQL Server\MSSQL.1\MSSQL\DATA\LabPractiseDB_log.ldf' , SIZE = 1024KB , FILEGROWTH = 10%)
GO
EXEC dbo.sp_dbcmptlevel @dbname=N'LabPractiseDB', @new_cmptlevel=90
GO
ALTER DATABASE [LabPractiseDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [LabPractiseDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [LabPractiseDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [LabPractiseDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [LabPractiseDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [LabPractiseDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [LabPractiseDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [LabPractiseDB] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [LabPractiseDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [LabPractiseDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [LabPractiseDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [LabPractiseDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [LabPractiseDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [LabPractiseDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [LabPractiseDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [LabPractiseDB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [LabPractiseDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [LabPractiseDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [LabPractiseDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [LabPractiseDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [LabPractiseDB] SET  READ_WRITE 
GO
ALTER DATABASE [LabPractiseDB] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [LabPractiseDB] SET  MULTI_USER 
GO
ALTER DATABASE [LabPractiseDB] SET PAGE_VERIFY CHECKSUM  
GO
USE [LabPractiseDB]
GO
IF NOT EXISTS (SELECT name FROM sys.filegroups WHERE is_default=1 AND name = N'PRIMARY') ALTER DATABASE [LabPractiseDB] MODIFY FILEGROUP [PRIMARY] DEFAULT
GO


USE LabPractiseDB;
------------------------------------------------------------------------
--Student 
------------------------------------------------------------------------
CREATE TABLE Student (
	StudentId int IDENTITY(1,1) NOT NULL PRIMARY KEY,
	FirstName varchar(50),
	LastName varchar(50),
	BirthDate smalldatetime,
	BirthPlace varchar(50),
	Address varchar(50),
	Phone varchar(20)
);
------------------------------------------------------------------------
--Department 
------------------------------------------------------------------------
CREATE TABLE Department (
	DepartmentId int IDENTITY(1,1) NOT NULL PRIMARY KEY,
	DepartmentName varchar(100) NOT NULL
);
------------------------------------------------------------------------
--Course 
------------------------------------------------------------------------
CREATE TABLE Course (
	CourseId int IDENTITY(1,1) NOT NULL PRIMARY KEY,
	CourseName varchar(50) NOT NULL,
	ShortName varchar(50),
	Year int NOT NULL,
	Description varchar(50),
	DepartmentId int NOT NULL,
	Description2 varchar(1000)
);

ALTER TABLE Course
ADD CONSTRAINT FK_Course_Department FOREIGN KEY (DepartmentId)
REFERENCES dbo.Department (DepartmentId);
------------------------------------------------------------------------
--StudentCourse 
------------------------------------------------------------------------
CREATE TABLE StudentCourse (
	StudentCourseId int IDENTITY(1,1) NOT NULL PRIMARY KEY,
	StudentId int NOT NULL,
	CourseId int NOT NULL
);

ALTER TABLE StudentCourse 
ADD CONSTRAINT FK_StudentCourse_Student FOREIGN KEY (StudentId)
REFERENCES dbo.Student (StudentId);

ALTER TABLE StudentCourse 
ADD CONSTRAINT FK_StudentCourse_Course FOREIGN KEY (CourseId)
REFERENCES dbo.Course (CourseId);
------------------------------------------------------------------------
--Professor 
------------------------------------------------------------------------
CREATE TABLE Professor (
	ProfessorId int IDENTITY(1,1) NOT NULL PRIMARY KEY,
	FirstName varchar(50) NOT NULL,
	LastName varchar(50) NOT NULL,
	ProfessorRank varchar(50) NOT NULL
);
------------------------------------------------------------------------
--ProfessorCourse
------------------------------------------------------------------------
CREATE TABLE ProfessorCourse(
	ProfessorCourseId int IDENTITY(1,1) NOT NULL PRIMARY KEY,
	ProfessorId int NOT NULL,
	CourseId int NOT NULL
);

ALTER TABLE ProfessorCourse
ADD CONSTRAINT FK_ProfessorCourse_Course FOREIGN KEY (CourseId)
REFERENCES dbo.Course (CourseId);

ALTER TABLE ProfessorCourse
ADD CONSTRAINT FK_Professor_Course_Professor FOREIGN KEY (ProfessorId)
REFERENCES dbo.Professor (ProfessorId);


--sec part

------------------------------------------------------------------------
--GroupForLab 
------------------------------------------------------------------------
CREATE TABLE GroupForLab (
	GroupForLabId int IDENTITY(1,1) NOT NULL PRIMARY KEY,
	CourseId int,
	DescriptionGroup VARCHAR(256),
	NoGroup tinyint NOT NULL
);
/*
ALTER TABLE GroupForLab
ADD DateTimeStart datetime NOT NULL DEFAULT (0);
ALTER TABLE GroupForLab
ADD DurationMinutes int NOT NULL DEFAULT (90);
*/
ALTER TABLE GroupForLab
ADD CONSTRAINT FK_GroupForLab_Course FOREIGN KEY (CourseId)
REFERENCES dbo.Course (CourseId);
------------------------------------------------------------------------
--StudentGroup 
------------------------------------------------------------------------
CREATE TABLE StudentGroup (
	StudentGroupId int IDENTITY(1,1) NOT NULL PRIMARY KEY,
	StudentId int NOT NULL,
	GroupForLabId int NOT NULL
);

ALTER TABLE StudentGroup
ADD CONSTRAINT FK_StudentGroup_Student FOREIGN KEY (StudentId)
REFERENCES dbo.Student (StudentId);

ALTER TABLE StudentGroup
ADD CONSTRAINT FK_StudentGroup_GroupForLab FOREIGN KEY (GroupForLabId)
REFERENCES dbo.GroupForLab (GroupForLabId);
------------------------------------------------------------------------
--ProfessorGroup 
------------------------------------------------------------------------
CREATE TABLE ProfessorGroup (
	ProfessorGroupId int IDENTITY(1,1) NOT NULL PRIMARY KEY,
	ProfessorId int NOT NULL,
	GroupForLabId int NOT NULL
);

ALTER TABLE ProfessorGroup 
ADD CONSTRAINT FK_ProfessorGroup_GroupForLab FOREIGN KEY (GroupForLabId)
REFERENCES dbo.GroupForLab (GroupForLabId);

ALTER TABLE ProfessorGroup
ADD CONSTRAINT FK_ProfessorGroup_Professor FOREIGN KEY (ProfessorId)
REFERENCES dbo.Professor (ProfessorId);

--third

CREATE TABLE Schedule (
	ScheduleId int IDENTITY(1,1) NOT NULL PRIMARY KEY,
	GroupForLabId int NOT NULL,
	NumLab int NOT NULL,
	DateTimeStart datetime NOT NULL DEFAULT (0),
	DurationMinutes int NOT NULL DEFAULT (90),
	Room varchar(100)
);

ALTER TABLE Schedule
ADD CONSTRAINT FK_Schedule_GroupForLab FOREIGN KEY (GroupForLabId)
REFERENCES dbo.GroupForLab (GroupForLabId);

--forth

CREATE TABLE Rating (
	RatingId int IDENTITY(1,1) NOT NULL PRIMARY KEY,
	StudentId int NOT NULL,
	ProfessorId int NOT NULL,
	CourseId int NOT NULL,
	GroupForLabId int NOT NULL,
	ScheduleId int NOT NULL,
	Rate int NOT NULL,
	Comment varchar(256)
);

ALTER TABLE Rating 
ADD CONSTRAINT FK_Rating_Student FOREIGN KEY (StudentId)
REFERENCES dbo.Student (StudentId);

ALTER TABLE Rating
ADD CONSTRAINT FK_Rating_Professor FOREIGN KEY (ProfessorId)
REFERENCES dbo.Professor (ProfessorId);

ALTER TABLE Rating
ADD CONSTRAINT FK_Rating_Course FOREIGN KEY (CourseId)
REFERENCES dbo.Course (CourseId);

ALTER TABLE Rating
ADD CONSTRAINT FK_Rating_GroupForLab FOREIGN KEY (GroupForLabId)
REFERENCES dbo.GroupForLab (GroupForLabId);

ALTER TABLE Rating
ADD CONSTRAINT FK_Rating_Schedule FOREIGN KEY (ScheduleId)
REFERENCES dbo.Schedule (ScheduleId);

--fifth

------------------------------------------------------------------------
--ProfessorRating 
------------------------------------------------------------------------
CREATE TABLE ProfessorRating (
	ProfessorRatingId int IDENTITY(1,1) NOT NULL PRIMARY KEY,
	StudentId int NOT NULL,
	ProfessorId int NOT NULL,
	Rate int NOT NULL,
	VersionRate int NOT NULL,
	DateRating datetime
);

ALTER TABLE ProfessorRating 
ADD CONSTRAINT FK_ProfessorRating_Student FOREIGN KEY (StudentId)
REFERENCES dbo.Student (StudentId);


ALTER TABLE ProfessorRating
ADD CONSTRAINT FK_ProfessorRating_Professor FOREIGN KEY (ProfessorId)
REFERENCES dbo.Professor (ProfessorId);
------------------------------------------------------------------------
--StudentDepartment 
------------------------------------------------------------------------
CREATE TABLE StudentDepartment (
	StudentDepartmentId int IDENTITY(1,1) NOT NULL PRIMARY KEY,
	StudentId int NOT NULL,
	DepartmentId int NOT NULL
);

ALTER TABLE StudentDepartment 
ADD CONSTRAINT FK_StudentDepartment_Student FOREIGN KEY (StudentId)
REFERENCES dbo.Student (StudentId);

ALTER TABLE StudentDepartment
ADD CONSTRAINT FK_StudentDepartment_Department FOREIGN KEY (DepartmentId)
REFERENCES dbo.Department (DepartmentId);
------------------------------------------------------------------------
--ProfessorDepartment 
------------------------------------------------------------------------
CREATE TABLE ProfessorDepartment (
	ProfessorDepartmentId int IDENTITY(1,1) NOT NULL PRIMARY KEY,
	ProfessorId int NOT NULL,
	DepartmentId int NOT NULL	
);

ALTER TABLE ProfessorDepartment 
ADD CONSTRAINT FK_ProfessorDepartment_Professor FOREIGN KEY (ProfessorId)
REFERENCES dbo.Professor (ProfessorId);

ALTER TABLE ProfessorDepartment
ADD CONSTRAINT FK_ProfessorDepartment_Department FOREIGN KEY (DepartmentId)
REFERENCES dbo.Department (DepartmentId);
------------------------------------------------------------------------
--Lecture
------------------------------------------------------------------------
CREATE TABLE Lecture (
	LectureId int IDENTITY(1,1) NOT NULL PRIMARY KEY,
	LectureName varchar(256) NOT NULL,
	LectureDescription varchar(256),
	Link varchar(256),
	CourseId int NOT NULL
);

ALTER TABLE Lecture
ADD CONSTRAINT FK_Lecture_Course FOREIGN KEY (CourseId)
REFERENCES dbo.Course (CourseId);
------------------------------------------------------------------------
--LabTask
------------------------------------------------------------------------
CREATE TABLE LabTask (
	LabTaskId int IDENTITY(1,1) NOT NULL PRIMARY KEY,
	LabTaskName varchar(256) NOT NULL,
	LabTaskDescription varchar(256),
	Link varchar(256),
	CourseId int NOT NULL
);

ALTER TABLE LabTask
ADD CONSTRAINT FK_LabTask_Course FOREIGN KEY (CourseId)
REFERENCES dbo.Course (CourseId);
------------------------------------------------------------------------
--LabPreparation 
------------------------------------------------------------------------
CREATE TABLE LabPreparation (
	LabPreparationId int IDENTITY(1,1) NOT NULL PRIMARY KEY,
	LabPreparationName varchar(256) NOT NULL,
	LabPreparationDescription varchar(256),
	Link varchar(256),
	CourseId int NOT NULL
);

ALTER TABLE LabPreparation
ADD CONSTRAINT FK_LabPreparation_Course FOREIGN KEY (CourseId)
REFERENCES dbo.Course (CourseId);
------------------------------------------------------------------------
--Test 
------------------------------------------------------------------------
CREATE TABLE Test (
	TestId int IDENTITY(1,1) NOT NULL PRIMARY KEY,
	TestName varchar(256) NOT NULL,
	TestDescription varchar(256),
	TestNum int,
	CourseId int NOT NULL
);

ALTER TABLE Test
ADD CONSTRAINT FK_Test_Course FOREIGN KEY (CourseId)
REFERENCES dbo.Course (CourseId);
------------------------------------------------------------------------
--Question
------------------------------------------------------------------------
CREATE TABLE Question (
	QuestionId int IDENTITY(1,1) NOT NULL PRIMARY KEY,
	QuestionText varchar(512) NOT NULL,
	QuestionType varchar(32) NOT NULL,
	QuestionWeight int CHECK (QuestionWeight > 0 AND QuestionWeight < 11),
	TestId int NOT NULL
);

ALTER TABLE Question
ADD CONSTRAINT FK_Question_Test FOREIGN KEY (TestId)
REFERENCES dbo.Test (TestId);
------------------------------------------------------------------------
--Answer 
------------------------------------------------------------------------
CREATE TABLE Answer (
	AnswerId int IDENTITY(1,1) NOT NULL PRIMARY KEY,
	AnswerValue varchar(256) NOT NULL,
	IsTrueAnswer int NOT NULL,
	QuestionId int NOT NULL
);

ALTER TABLE Answer
ADD CONSTRAINT FK_Answer_Question FOREIGN KEY (QuestionId)
REFERENCES dbo.Question (QuestionId);
------------------------------------------------------------------------
--StudentTestResult
------------------------------------------------------------------------
CREATE TABLE StudentTestResult (
	StudentTestResultId int IDENTITY(1,1) NOT NULL PRIMARY KEY,
	StudentId int NOT NULL,
	TestId int NOT NULL,
	ResultPercent int NOT NULL,
	DateResult datetime
);

ALTER TABLE StudentTestResult
ADD CONSTRAINT FK_StudentTestResult_Student FOREIGN KEY (StudentId)
REFERENCES dbo.Student (StudentId);

ALTER TABLE StudentTestResult
ADD CONSTRAINT FK_StudentTestResult_Test FOREIGN KEY (TestId)
REFERENCES dbo.Test (TestId);