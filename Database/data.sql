--DATA
USE LabPractiseDB;
------------------------------------------------------------------------
--Student 
------------------------------------------------------------------------
INSERT INTO Student VALUES ('John', 'Smith', '9/3/1995', 'Nis', 'Street 1', '123456789');
INSERT INTO Student VALUES ('Oliver', 'Jones', '8/4/1994', 'Nis', 'Street 1', '123456789');
INSERT INTO Student VALUES ('Harry', 'Williams', '7/5/1994', 'Nis', 'Street 1', '123456789');
INSERT INTO Student VALUES ('Jack', 'Taylor', '6/6/1993', 'Nis', 'Street 1', '123456789');
INSERT INTO Student VALUES ('George', 'Davies', '5/7/1993', 'Nis', 'Street 1', '123456789');
INSERT INTO Student VALUES ('Olivia', 'Jones', '4/8/1994', 'Nis', 'Street 1', '123456789');
INSERT INTO Student VALUES ('Amelia', 'Brown', '3/9/1994', 'Nis', 'Street 1', '123456789');
INSERT INTO Student VALUES ('Isla', 'Wilson', '2/1/1995', 'Nis', 'Street 1', '123456789');
INSERT INTO Student VALUES ('Emily', 'Simic', '1/2/1995', 'Nis', 'Street 1', '123456789');
INSERT INTO Student VALUES ('Ava', 'Jovanovic', '9/3/1994', 'Nis', 'Street 1', '123456789');
INSERT INTO Student VALUES ('John', 'Jovanovic', '8/4/1994', 'Nis', 'Street 1', '123456789');
INSERT INTO Student VALUES ('Oliver', 'Simic', '7/5/1993', 'Nis', 'Street 1', '123456789');
INSERT INTO Student VALUES ('Harry', 'Wilson', '6/6/1993', 'Nis', 'Street 1', '123456789');
INSERT INTO Student VALUES ('Jack', 'Brown', '5/7/1994', 'Nis', 'Street 1', '123456789');
INSERT INTO Student VALUES ('George', 'Jones', '4/8/1995', 'Nis', 'Street 1', '123456789');
INSERT INTO Student VALUES ('Olivia', 'Davies', '3/1/1995', 'Nis', 'Street 1', '123456789');
INSERT INTO Student VALUES ('Amelia', 'Taylor', '2/3/1995', 'Nis', 'Street 1', '123456789');
INSERT INTO Student VALUES ('Isla', 'Williams', '2/3/1995', 'Nis', 'Street 1', '123456789');
INSERT INTO Student VALUES ('Emily', 'Jones', '8/4/1995', 'Nis', 'Street 1', '123456789');
INSERT INTO Student VALUES ('Ava', 'Smith', '9/5/1995', 'Nis', 'Street 1', '123456789');

------------------------------------------------------------------------
--Department 
------------------------------------------------------------------------

INSERT INTO Department (DepartmentName) VALUES ('COMPUTER SCIENCE');
INSERT INTO Department (DepartmentName) VALUES ('MICROELECTRONICS');
INSERT INTO Department (DepartmentName) VALUES ('TELECOMMUNICATIONS');
INSERT INTO Department (DepartmentName) VALUES ('POWER ENGINEERING');

------------------------------------------------------------------------
--Course 
------------------------------------------------------------------------

INSERT INTO Course (CourseName, ShortName, Year, DepartmentId, Description2) VALUES ('Advanced Databases', 'AD', 4, 1,'This course is a comprehensive study of the internals of modern database management systems. It will cover the core concepts and fundamentals of the components that are used in both high-performance transaction processing systems (OLTP) and large-scale analytical systems (OLAP). The class will stress both efficiency and correctness of the implementation of these ideas. All class projects will be in the context of a real in-memory, multi-core database system. The course is appropriate for graduate students in software systems and for advanced undergraduates with dirty systems programming skills.');
INSERT INTO Course (CourseName, ShortName, Year, DepartmentId, Description2) VALUES ('Artificial Intelligence', 'AI', 4, 1, 'Artificial intelligence (AI), sometimes called machine intelligence, is intelligence demonstrated by machines, in contrast to the natural intelligence displayed by humans and other animals. In computer science AI research is defined as the study of intelligent agents: any device that perceives its environment and takes actions that maximize its chance of successfully achieving its goals. Colloquially, the term artificial intelligence is applied when a machine mimics cognitive functions that humans associate with other human minds, such as learning and problem solving');
INSERT INTO Course (CourseName, ShortName, Year, DepartmentId, Description2) VALUES ('Data Mining', 'DM', 4, 1, 'Data mining is the process of discovering patterns in large data sets involving methods at the intersection of machine learning, statistics, and database systems. An interdisciplinary subfield of computer science, it is an essential process � wherein intelligent methods are applied to extract data patterns � the overall goal of which is to extract information from a data set, and transform it into an understandable structure for further use. Aside from the raw analysis step, it involves database and data management aspects, data pre-processing, model and inference considerations, interestingness metrics, complexity considerations, post-processing of discovered structures, visualization, and online updating. Data mining is the analysis step of the knowledge discovery in databases process, or KDD.');

------------------------------------------------------------------------
--Professor 
------------------------------------------------------------------------

INSERT INTO Professor (FirstName, LastName, ProfessorRank) VALUES ('Linus','Torvalds','Professor');
INSERT INTO Professor (FirstName, LastName, ProfessorRank) VALUES ('Mark','Zuckerberg','Assistant Professor');
INSERT INTO Professor (FirstName, LastName, ProfessorRank) VALUES ('Ada','Lovelace','Docent');
INSERT INTO Professor (FirstName, LastName, ProfessorRank) VALUES ('Ken','Thompson','Professor');
INSERT INTO Professor (FirstName, LastName, ProfessorRank) VALUES ('Grace','Hopper','Assistant Professor');
INSERT INTO Professor (FirstName, LastName, ProfessorRank) VALUES ('Donald','Knuth','Docent');
INSERT INTO Professor (FirstName, LastName, ProfessorRank) VALUES ('Richard','Stallman','Professor');
INSERT INTO Professor (FirstName, LastName, ProfessorRank) VALUES ('John','Carmack','Assistant Professor');